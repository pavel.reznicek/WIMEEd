#!/bin/sh

set -v
set -e
cd "/home/pavel/Tvorba/WIME/WIMEEd"
mkdir -p /tmp/wimeed-1.0.0.0
cp *.lpi /tmp/wimeed-1.0.0.0/
cp *.lpr /tmp/wimeed-1.0.0.0/
cp *.pas /tmp/wimeed-1.0.0.0/
cp *.lfm /tmp/wimeed-1.0.0.0/
mkdir -p /tmp/wimeed-1.0.0.0/db
cp db/*.pas /tmp/wimeed-1.0.0.0/db/
cp db/*.lfm /tmp/wimeed-1.0.0.0/db/
cp *.ico /tmp/wimeed-1.0.0.0/
cp *.md /tmp/wimeed-1.0.0.0/
cp locale/*.po /tmp/wimeed-1.0.0.0/

cd /tmp/wimeed-1.0.0.0
rm -rf DEBUILD
rm -f DEBUILD.sh

cd ..
tar czf wimeed_1.0.0.0.orig.tar.gz wimeed-1.0.0.0
mv wimeed-1.0.0.0 "/home/pavel/Tvorba/WIME/WIMEEd/DEBUILD"
mv wimeed_1.0.0.0.orig.tar.gz "/home/pavel/Tvorba/WIME/WIMEEd/DEBUILD"

cd "/home/pavel/Tvorba/WIME/WIMEEd/DEBUILD/wimeed-1.0.0.0"
mkdir -p debian/source
echo "1.0" > debian/source/format
echo "9" > debian/compat
mv ../control debian/
mv ../rules debian/
chmod +x debian/rules
mv ../changelog debian/
mv ../copyright debian/
debuild -us -uc -S
cd ..
xterm -e "debsign *.changes"
dput ppa:cigydd/wimeed *.changes
