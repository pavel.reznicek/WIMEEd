;WIMEEd Installer Script
;Template:
;NSIS Modern User Interface
;Multilingual Example Script
;Written by Joost Verburg

!pragma warning error all

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;Include the LogicLib and x64 headers

  !include LogicLib.nsh
  !include x64.nsh

;--------------------------------

;General

  ;Properly display all languages (Installer will not work on Windows 95, 98 or ME!)
  Unicode true

  !include "version.nsh"
  
  ;Name and file
  !define PRODUCT "WIMEEd"
  !define LPRODUCT "wimeed"
  Name "${PRODUCT} v. ${VERSION}"
  OutFile "Releases\${LPRODUCT}-${VERSION}-${ARCH}-${OS}.exe"

  ;Default installation folder
  ;InstallDir "$PROGRAMFILES\${PRODUCT}"
  
  ;Get installation folder from registry if available
  ;InstallDirRegKey HKCU "Software\${PRODUCT}" ""

  ;Request application privileges for Windows Vista
  ;RequestExecutionLevel user
  RequestExecutionLevel admin

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

  ;Show all languages, despite user's codepage
  !define MUI_LANGDLL_ALLLANGUAGES

;--------------------------------
;Language Selection Dialog Settings

  ;Remember the installer language
  !define MUI_LANGDLL_REGISTRY_ROOT "HKLM"
  !define MUI_LANGDLL_REGISTRY_KEY "Software\${PRODUCT}"
  !define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"

;--------------------------------
;Desktop shortcut creation

  !define MUI_FINISHPAGE_RUN ""
  !define MUI_FINISHPAGE_RUN_TEXT "Create Desktop Shortcut"
  !define MUI_FINISHPAGE_RUN_FUNCTION finishpageaction

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "LICENSE.md"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_LICENSE "LICENSE.md"
  !insertmacro MUI_UNPAGE_COMPONENTS
  !insertmacro MUI_UNPAGE_DIRECTORY
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English" ; The first language is the default language
  ;!insertmacro MUI_LANGUAGE "French"
  !insertmacro MUI_LANGUAGE "German"
  ;!insertmacro MUI_LANGUAGE "Spanish"
  ;!insertmacro MUI_LANGUAGE "SpanishInternational"
  ;!insertmacro MUI_LANGUAGE "SimpChinese"
  ;!insertmacro MUI_LANGUAGE "TradChinese"
  ;!insertmacro MUI_LANGUAGE "Japanese"
  ;!insertmacro MUI_LANGUAGE "Korean"
  ;!insertmacro MUI_LANGUAGE "Italian"
  ;!insertmacro MUI_LANGUAGE "Dutch"
  ;!insertmacro MUI_LANGUAGE "Danish"
  ;!insertmacro MUI_LANGUAGE "Swedish"
  ;!insertmacro MUI_LANGUAGE "Norwegian"
  ;!insertmacro MUI_LANGUAGE "NorwegianNynorsk"
  ;!insertmacro MUI_LANGUAGE "Finnish"
  ;!insertmacro MUI_LANGUAGE "Greek"
  ;!insertmacro MUI_LANGUAGE "Russian"
  ;!insertmacro MUI_LANGUAGE "Portuguese"
  ;!insertmacro MUI_LANGUAGE "PortugueseBR"
  ;!insertmacro MUI_LANGUAGE "Polish"
  ;!insertmacro MUI_LANGUAGE "Ukrainian"
  !insertmacro MUI_LANGUAGE "Czech"
  !insertmacro MUI_LANGUAGE "Slovak"
  ;!insertmacro MUI_LANGUAGE "Croatian"
  ;!insertmacro MUI_LANGUAGE "Bulgarian"
  ;!insertmacro MUI_LANGUAGE "Hungarian"
  ;!insertmacro MUI_LANGUAGE "Thai"
  ;!insertmacro MUI_LANGUAGE "Romanian"
  ;!insertmacro MUI_LANGUAGE "Latvian"
  ;!insertmacro MUI_LANGUAGE "Macedonian"
  ;!insertmacro MUI_LANGUAGE "Estonian"
  ;!insertmacro MUI_LANGUAGE "Turkish"
  ;!insertmacro MUI_LANGUAGE "Lithuanian"
  ;!insertmacro MUI_LANGUAGE "Slovenian"
  ;!insertmacro MUI_LANGUAGE "Serbian"
  ;!insertmacro MUI_LANGUAGE "SerbianLatin"
  ;!insertmacro MUI_LANGUAGE "Arabic"
  ;!insertmacro MUI_LANGUAGE "Farsi"
  ;!insertmacro MUI_LANGUAGE "Hebrew"
  ;!insertmacro MUI_LANGUAGE "Indonesian"
  ;!insertmacro MUI_LANGUAGE "Mongolian"
  ;!insertmacro MUI_LANGUAGE "Luxembourgish"
  ;!insertmacro MUI_LANGUAGE "Albanian"
  ;!insertmacro MUI_LANGUAGE "Breton"
  ;!insertmacro MUI_LANGUAGE "Belarusian"
  ;!insertmacro MUI_LANGUAGE "Icelandic"
  ;!insertmacro MUI_LANGUAGE "Malay"
  ;!insertmacro MUI_LANGUAGE "Bosnian"
  ;!insertmacro MUI_LANGUAGE "Kurdish"
  ;!insertmacro MUI_LANGUAGE "Irish"
  ;!insertmacro MUI_LANGUAGE "Uzbek"
  ;!insertmacro MUI_LANGUAGE "Galician"
  ;!insertmacro MUI_LANGUAGE "Afrikaans"
  ;!insertmacro MUI_LANGUAGE "Catalan"
  ;!insertmacro MUI_LANGUAGE "Esperanto"
  ;!insertmacro MUI_LANGUAGE "Asturian"
  ;!insertmacro MUI_LANGUAGE "Basque"
  ;!insertmacro MUI_LANGUAGE "Pashto"
  ;!insertmacro MUI_LANGUAGE "ScotsGaelic"
  ;!insertmacro MUI_LANGUAGE "Georgian"
  ;!insertmacro MUI_LANGUAGE "Vietnamese"
  ;!insertmacro MUI_LANGUAGE "Welsh"
  ;!insertmacro MUI_LANGUAGE "Armenian"
  ;!insertmacro MUI_LANGUAGE "Corsican"
  ;!insertmacro MUI_LANGUAGE "Tatar"
  ;!insertmacro MUI_LANGUAGE "Hindi"

;--------------------------------
;Reserve Files
  
  ;If you are using solid compression, files that are required before
  ;the actual installation should be stored first in the data block,
  ;because this will make your installer start faster.
  
  !insertmacro MUI_RESERVEFILE_LANGDLL

;--------------------------------
;Installer Sections

Section "Core Files" SecCore

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  File "${LPRODUCT}.exe"
  File "LICENSE.md"
  File "README.md"
  
  ;Store installation folder
  ;WriteRegStr HKCU "Software\${PRODUCT}" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

Section "Language Files" SecLang
        ;Make the locale directory
        SetOutPath "$INSTDIR\locale"
        File "locale\*.po"
SectionEnd

;--------------------------------
;Installer Functions

Function .onInit

  ${If} ${RunningX64}
    ${If} ${ARCH} == "x86_64"
      StrCpy $InstDir "$PROGRAMFILES64\${PRODUCT}"
    ${Else}
      StrCpy $InstDir "$PROGRAMFILES32\${PRODUCT}"
    ${Endif}
  ${Else}
    ${If} ${ARCH} == "x86_64"
      Quit
    ${Else}
      StrCpy $InstDir "$PROGRAMFILES\${PRODUCT}"
    ${Endif}
  ${EndIf}

  !insertmacro MUI_LANGDLL_DISPLAY

FunctionEnd

Function FinishPageAction
  CreateShortcut "$DESKTOP\${PRODUCT}.lnk" "$INSTDIR\${LPRODUCT}.exe"
FunctionEnd

;--------------------------------
;Descriptions

  ;USE A LANGUAGE STRING IF YOU WANT YOUR DESCRIPTIONS TO BE LANGAUGE SPECIFIC
  LangString DESC_SecCore   ${LANG_ENGLISH} "Core files."
  LangString DESC_SecCore   ${LANG_GERMAN}  "Haupte Dateien."
  LangString DESC_SecCore   ${LANG_CZECH}   "Hlavn� soubory."
  LangString DESC_SecCore   ${LANG_SLOVAK}  "Hlavn� s�bory."
  LangString DESC_SecLang   ${LANG_ENGLISH} "Language files."
  LangString DESC_SecLang   ${LANG_GERMAN}  "Sprachendateien."
  LangString DESC_SecLang   ${LANG_CZECH}   "Jazykov� soubory."
  LangString DESC_SecLang   ${LANG_SLOVAK}  "Jazykov� s�bory."

  ;Assign descriptions to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecCore} $(DESC_SecCore)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecLang} $(DESC_SecLang)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall" SecUn

  Delete "$DESKTOP\${PRODUCT}.lnk"

  Delete "$INSTDIR\locale\*.po"
  RMDir "$INSTDIR\locale"

  Delete "$INSTDIR\${LPRODUCT}.exe"

  Delete "$INSTDIR\LICENSE.md"
  Delete "$INSTDIR\README.md"

  Delete "$INSTDIR\Uninstall.exe"

  RMDir "$INSTDIR"
  
  DeleteRegKey /ifempty HKLM "Software\${PRODUCT}"

SectionEnd

;----------------------------------------------
;The uninstall section description_
  LangString DESC_SecUn ${LANG_ENGLISH} "Uninstall."
  LangString DESC_SecUn ${LANG_GERMAN}  "Deinstallieren."
  LangString DESC_SecUn ${LANG_CZECH}   "Odinstallovat."
  LangString DESC_SecUn ${LANG_SLOVAK}  "Odin�talova�."

  !insertmacro MUI_UNFUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecUn} $(DESC_SecUn)
  !insertmacro MUI_UNFUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Functions

Function un.onInit

  !insertmacro MUI_UNGETLANGUAGE
  
FunctionEnd