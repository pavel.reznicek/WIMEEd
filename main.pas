unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Dialogs, ExtCtrls, ComCtrls,
  StdCtrls, Spin,
  WIMEUtils,
  WIMECityNameDB, WIMECityNameDBObj,
  WIMEMapIconDB, WIMEMapIconDBObj,
  WIMEInventoryObjectDBObj,
  WIMEForceNameDB, WIMEForceNameDBObj,
  BufDataset, xmldatapacketreader, DefaultTranslator;

type
  { TFMain }

  TFMain = class(TForm)
    gbForceProperties: TGroupBox;
    gbForceList: TGroupBox;
    ilButtons: TImageList;
    lbCount: TLabel;
    lbFollowValue: TLabel;
    lbInventoryObjects: TLabel;
    lbStealth: TLabel;
    lbSpriteColorScheme: TLabel;
    lbMapIcon: TLabel;
    lbHitPointsQuantity: TLabel;
    lbDestinationX: TLabel;
    lbLocationY: TLabel;
    lbDestinationY: TLabel;
    lbVisibilityIndicator: TLabel;
    lbSpriteType: TLabel;
    lbMoraleTotal: TLabel;
    lbCountAlive: TLabel;
    lbMoraleQuantity: TLabel;
    lbHitPointsTotal: TLabel;
    lbLocationX: TLabel;
    lbPowerLevel: TLabel;
    lbArmyMobilizeValue: TLabel;
    OD: TOpenDialog;
    panForceList: TPanel;
    panForce: TPanel;
    SD: TSaveDialog;
    seCount: TSpinEdit;
    seFollowValue: TSpinEdit;
    seInventoryObjects: TSpinEdit;
    seStealth: TSpinEdit;
    seSpriteColorScheme: TSpinEdit;
    seMapIcon: TSpinEdit;
    seHitPointsQuantity: TSpinEdit;
    seDestinationX: TSpinEdit;
    seLocationY: TSpinEdit;
    seDestinationY: TSpinEdit;
    seVisibilityIndicator: TSpinEdit;
    seSpriteType: TSpinEdit;
    seMoraleTotal: TSpinEdit;
    seCountAlive: TSpinEdit;
    seMoraleQuantity: TSpinEdit;
    seHitPointsTotal: TSpinEdit;
    seLocationX: TSpinEdit;
    sePowerLevel: TSpinEdit;
    seArmyMobilizeValue: TSpinEdit;
    spTreeView: TSplitter;
    ToolBar: TToolBar;
    tbSave: TToolButton;
    tbOpen: TToolButton;
    tvForces: TTreeView;
    cbLocation: TCityNameDBCombo;
    cbDestination: TCityNameDBCombo;
    cbMapIcon: TMapIconNameDBCombo;
    cbInventoryObjects: TInventoryObjectsDBCombo;
    cbFollowValue: TForceNameDBCombo;
    procedure FormCreate(Sender: TObject);
    procedure cbLocationChange(Sender: TObject);
    procedure cbDestinationChange(Sender: TObject);
    procedure cbInventoryObjectsChange(Sender: Tobject);
    procedure cbMapIconChange(Sender: TObject);
    procedure cbFollowValueChange(Sender: TObject);
    procedure gbForcePropertiesResize(Sender: TObject);
    procedure seArmyMobilizeValueChange(Sender: TObject);
    procedure seCountChange(Sender: TObject);
    procedure seCountAliveChange(Sender: TObject);
    procedure seDestinationXChange(Sender: TObject);
    procedure seDestinationYChange(Sender: TObject);
    procedure seFollowValueChange(Sender: TObject);
    procedure seHitPointsQuantityChange(Sender: TObject);
    procedure seHitPointsTotalChange(Sender: TObject);
    procedure seInventoryObjectsChange(Sender: TObject);
    procedure seLocationXChange(Sender: TObject);
    procedure seLocationYChange(Sender: TObject);
    procedure seMapIconChange(Sender: TObject);
    procedure seMoraleQuantityChange(Sender: TObject);
    procedure seMoraleTotalChange(Sender: TObject);
    procedure sePowerLevelChange(Sender: TObject);
    procedure seSpriteColorSchemeChange(Sender: TObject);
    procedure seSpriteTypeChange(Sender: TObject);
    procedure seStealthChange(Sender: TObject);
    procedure seVisibilityIndicatorChange(Sender: TObject);
    procedure tbOpenClick(Sender: TObject);
    procedure tbSaveClick(Sender: TObject);
    procedure tvForcesChange(Sender: TObject; Node: TTreeNode);
  private
    { private declarations }
    FLittleEndianForces: TLittleEndianForces;
    FBigEndianForces: TBigEndianForces;
    FForceIndex: Integer;
    FFileName: String;
    FEndian: TEndian;
    FPlatform: TPlatform;
    procedure AddCityCombos;
    procedure AddMapIconCombo;
    procedure AddInventoryObjectCombo;
    procedure AddFollowValueCombo;
    procedure UpdateLocation;
    procedure UpdateDestination;
    procedure UpdateInventoryObjects;
    procedure UpdateMapIcon;
    procedure UpdateFollowValue;
    function GetControlsEnabled: Boolean;
    procedure SetControlsEnabled(Enable: Boolean);
    procedure Open;
    procedure Save;
  public
    { public declarations }
    property FileName: String read FFileName write FFileName;
    property ControlsEnabled: Boolean read GetControlsEnabled
      write SetControlsEnabled;
  end;


const
  MonospaceFontName =
    {$IFDEF WINDOWS}
    'Courier New'
    {$ELSE}
    'Monospace'
    {$ENDIF};
resourcestring
  DefaultFilterWin =
    'WIME saved games (archive.dat)|archive.dat|' +
    'DAT files (*.dat)|*.dat|All files (*)|*';
  DefaultFilterLin =
    'WIME saved games (archive.dat)|[aA][rR][cC][hH][iI][vV][eE].[dD][aA][tT]|'+
    'DAT files (*.dat)|*.[dD][aA][tT]|'+
    'All files (*)|*';

  txtForceProperties = '&Force properties';

  txtChangesSaved = 'Your changes have been saved to file “%s”.';

const
  DefaultFilter: String =
    {$IFDEF WINDOWS}
    DefaultFilterWin
    {$ELSE}
    DefaultFilterLin
    {$ENDIF};

var
  FMain: TFMain;

  PlatformEndian: TPlatformEndian =
    (endLittle, endLittle, endBig, endBig, endLittle);


implementation

{$R *.lfm}



{ TFMain }

procedure TFMain.FormCreate(Sender: TObject);
begin
  FEndian := endLittle;
  AddCityCombos;
  AddMapIconCombo;
  AddInventoryObjectCombo;
  AddFollowValueCombo;
  ControlsEnabled := False;
  tvForces.Font.Name := MonospaceFontName;
  Constraints.MinWidth := Width;
  Constraints.MinHeight := Height;
end;

procedure TFMain.cbLocationChange(Sender: TObject);
var
  Loc: TPoint;
begin
  //Loc := cbLocation.LocationByName[cbLocation.Text];
  Loc := cbLocation.Location;
  if Loc <> DefaultLocation then
  begin
    seLocationX.Value := Loc.x;
    seLocationY.Value := Loc.y;
  end;
end;

procedure TFMain.cbDestinationChange(Sender: TObject);
var
  Loc: TPoint;
begin
  //Loc := cbDestination.LocationByName[cbLocation.Text];
  Loc := cbDestination.Location;
  if Loc <> DefaultLocation then
  begin
    seDestinationX.Value := Loc.x;
    seDestinationY.Value := Loc.y;
  end;
end;

procedure TFMain.cbInventoryObjectsChange(Sender: Tobject);
var
  InventoryObjects: Word;
begin
  InventoryObjects := cbInventoryObjects.InventoryObjectsAsWord;
  seInventoryObjects.Value := InventoryObjects;
end;

procedure TFMain.cbMapIconChange(Sender: TObject);
var
  GameID: Integer;
begin
  GameID := cbMapIcon.MapIconGameID;
  if GameID <> DefaultGameID then
    seMapIcon.Value := GameID;
end;

procedure TFMain.cbFollowValueChange(Sender: TObject);
var
  FollowValue: Integer;
begin
  FollowValue := cbFollowValue.ItemIndex;
  if FollowValue > -1 then seFollowValue.Value := FollowValue;
end;

procedure TFMain.gbForcePropertiesResize(Sender: TObject);
var
  HalfWidth: Integer;
begin
  HalfWidth := (gbForceProperties.ClientWidth - 3 * 8) div 2;
  if cbInventoryObjects <> nil then
    cbInventoryObjects.Width := HalfWidth;
  if cbMapIcon <> nil then
    cbMapIcon.Width := HalfWidth;
  if cbFollowValue <> nil then
    cbFollowValue.Width := HalfWidth;
end;

procedure TFMain.seArmyMobilizeValueChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].ArmyMobilizeValue :=
      seArmyMobilizeValue.Value;
  end;
end;

procedure TFMain.seCountChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].CountTotal := seCount.Value;
    seCountAlive.MaxValue := seCount.Value;
  end;
end;

procedure TFMain.seCountAliveChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].CountAlive := seCountAlive.Value;
  end;
end;

procedure TFMain.seDestinationXChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    cbDestination.Location := Point(seDestinationX.Value, seDestinationY.Value);
    FLittleEndianForces[FForceIndex].DestinationX := seDestinationX.Value;
  end;
end;

procedure TFMain.seDestinationYChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    cbDestination.Location := Point(seDestinationX.Value, seDestinationY.Value);
    FLittleEndianForces[FForceIndex].DestinationY := seDestinationY.Value;
  end;
end;

procedure TFMain.seFollowValueChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    cbFollowValue.ItemIndex := seFollowValue.Value;
    FLittleEndianForces[FForceIndex].FollowValue := seFollowValue.Value;
  end;
end;

procedure TFMain.seHitPointsQuantityChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].HitPointsQuantity :=
      seHitPointsQuantity.Value;
  end;
end;

procedure TFMain.seHitPointsTotalChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].HitPointsTotal := seHitPointsTotal.Value;
    seHitPointsQuantity.MaxValue := seHitPointsTotal.Value;
  end;
end;

procedure TFMain.seInventoryObjectsChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    cbInventoryObjects.InventoryObjectsAsWord := seInventoryObjects.Value;
    FLittleEndianForces[FForceIndex].InventoryObjects :=
      seInventoryObjects.Value;
  end;
end;

procedure TFMain.seLocationXChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    cbLocation.Location := Point(seLocationX.Value, seLocationY.Value);
    FLittleEndianForces[FForceIndex].LocationX := seLocationX.Value;
  end;
end;

procedure TFMain.seLocationYChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    cbLocation.Location := Point(seLocationX.Value, seLocationY.Value);
    FLittleEndianForces[FForceIndex].LocationY := seLocationY.Value;
  end;
end;

procedure TFMain.seMapIconChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].MapIcon := seMapIcon.Value;
    cbMapIcon.MapIconGameID := seMapIcon.Value;
  end;
end;

procedure TFMain.seMoraleQuantityChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].MoraleQuantity := seMoraleQuantity.Value;
  end;
end;

procedure TFMain.seMoraleTotalChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].MoraleTotal := seMoraleTotal.Value;
    seMoraleQuantity.MaxValue := seMoraleTotal.Value;
  end;
end;

procedure TFMain.sePowerLevelChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].PowerLevel :=
      sePowerLevel.Value;
  end;
end;

procedure TFMain.seSpriteColorSchemeChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].SpriteColorScheme :=
      seSpriteColorScheme.Value;
  end;
end;

procedure TFMain.seSpriteTypeChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].SpriteType := seSpriteType.Value;
  end;
end;

procedure TFMain.seStealthChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].Stealth := seStealth.Value;
  end;
end;

procedure TFMain.seVisibilityIndicatorChange(Sender: TObject);
begin
  if FForceIndex >= 0 then
  begin
    FLittleEndianForces[FForceIndex].VisibilityIndicator :=
      seVisibilityIndicator.Value;
  end;
end;

procedure TFMain.tbOpenClick(Sender: TObject);
var
  FilePath: String;
  FileNameWithoutPath: String;
begin
  FilePath := ExtractFilePath(FFileName);
  OD.Filter := DefaultFilter;
  OD.InitialDir := FilePath;
  FileNameWithoutPath := ExtractFileName(FFileName);
  OD.FileName := FileNameWithoutPath;
  if OD.Execute then
  begin
    FFileName := OD.FileName;
    Open;
  end;
end;

procedure TFMain.tbSaveClick(Sender: TObject);
var
  FilePath: String;
  FileNameWithoutPath: String;
begin
  FilePath := ExtractFilePath(FFileName);
  SD.Filter := DefaultFilter;
  SD.InitialDir := FilePath;
  FileNameWithoutPath := ExtractFileName(FFileName);
  SD.FileName := FileNameWithoutPath;
  if SD.Execute then
  begin
    FFileName := OD.FileName;
    Save;
  end;
end;

procedure TFMain.tvForcesChange(Sender: TObject; Node: TTreeNode);
begin
  if Node <> nil then
  begin
    FForceIndex := Node.Index;
    gbForceProperties.Caption := txtForceProperties + txtDash + Node.Text;
    seCount.Value := FLittleEndianForces[FForceIndex].CountTotal;
    seCountAlive.Value := FLittleEndianForces[FForceIndex].CountAlive;
    seLocationX.Value := FLittleEndianForces[FForceIndex].LocationX;
    seLocationY.Value := FLittleEndianForces[FForceIndex].LocationY;
    UpdateLocation;
    seDestinationX.Value := FLittleEndianForces[FForceIndex].DestinationX;
    seDestinationY.Value := FLittleEndianForces[FForceIndex].DestinationY;
    UpdateDestination;
    seInventoryObjects.Value :=
      FLittleEndianForces[FForceIndex].InventoryObjects;
    UpdateInventoryObjects;
    seMapIcon.Value := FLittleEndianForces[FForceIndex].MapIcon;
    UpdateMapIcon;
    seSpriteColorScheme.Value :=
      FLittleEndianForces[FForceIndex].SpriteColorScheme;
    seSpriteType.Value := FLittleEndianForces[FForceIndex].SpriteType;
    seVisibilityIndicator.Value :=
      FLittleEndianForces[FForceIndex].VisibilityIndicator;
    sePowerLevel.Value := FLittleEndianForces[FForceIndex].PowerLevel;
    seArmyMobilizeValue.Value :=
      FLittleEndianForces[FForceIndex].ArmyMobilizeValue;
    seStealth.Value := FLittleEndianForces[FForceIndex].Stealth;
    seMoraleTotal.Value := FLittleEndianForces[FForceIndex].MoraleTotal;
    seMoraleQuantity.Value := FLittleEndianForces[FForceIndex].MoraleQuantity;
    seHitPointsTotal.Value := FLittleEndianForces[FForceIndex].HitPointsTotal;
    seHitPointsQuantity.Value :=
      FLittleEndianForces[FForceIndex].HitPointsQuantity;
    seFollowValue.Value := FLittleEndianForces[FForceIndex].FollowValue;
    UpdateFollowValue;
  end;
end;

{ This procedure adds the city name combos to the form programmatically.
  Package deployment would be too difficult on Launchpad so we have to do this
  such a way.
}
procedure TFMain.AddCityCombos;
begin
  // Test the CityNameDB object
  //CityNameDB.SaveToFile('CityNameDBTest.xml', dfXML);
  // Create the combos
  cbLocation := TCityNameDBCombo.Create(Self);
  cbDestination := TCityNameDBCombo.Create(Self);
  // Give them a parent
  cbLocation.Parent := gbForceProperties;
  cbDestination.Parent := gbForceProperties;
  // Anchor them to the groupboxe's borders
  cbLocation.AnchorParallel(akLeft, 8, gbForceProperties);
  cbDestination.AnchorParallel(akLeft, 8, gbForceProperties);
  cbLocation.AnchorParallel(akRight, 8, gbForceProperties);
  cbDestination.AnchorParallel(akRight, 8, gbForceProperties);
  // Insert them into the SpinEdit chains
  cbLocation.AnchorToNeighbour(akTop, 5, seLocationX);
  seDestinationX.AnchorToNeighbour(akTop, 5, cbLocation);
  seDestinationY.AnchorToNeighbour(akTop, 5, cbLocation);
  cbDestination.AnchorToNeighbour(akTop, 5, seDestinationX);
  seInventoryObjects.AnchorToNeighbour(akTop, 5, cbDestination);
  seMapIcon.AnchorToNeighbour(akTop, 5, cbDestination);
  // Arrange the tab order
  cbLocation.TabOrder := seLocationY.TabOrder + 1;
  cbDestination.TabOrder := seDestinationY.TabOrder + 1;
  // Assign the event handlers
  cbLocation.OnChange := @cbLocationChange;
  cbDestination.OnChange := @cbDestinationChange;
end;

procedure TFMain.AddMapIconCombo;
begin
  // Test the MapIconNameDB object
  //MapIconNameDB.SaveToFile('MapIconNameDBTest.xml', dfXML);
  // Create the combo
  cbMapIcon := TMapIconNameDBCombo.Create(Self);
  // Give it a parent
  cbMapIcon.Parent := gbForceProperties;
  // Anchor it to the groupboxe's borders
  cbMapIcon.AnchorParallel(akRight, 8, gbForceProperties);
  cbMapIcon.Anchors := cbMapIcon.Anchors - [akLeft];
  cbMapIcon.Width := (gbForceProperties.ClientWidth - 3 * 8) div 2;
  // Insert it into the SpinEdit chains
  cbMapIcon.AnchorToNeighbour(akTop, 5, seMapIcon);
  seSpriteType.AnchorToNeighbour(akTop, 5, cbMapIcon);
  // Arrange the tab order
  cbMapIcon.TabOrder := seMapIcon.TabOrder + 1;
  // Assign the event handler
  cbMapIcon.OnChange := @cbMapIconChange;
end;

procedure TFMain.AddInventoryObjectCombo;
begin
  // Create the combo
  cbInventoryObjects := TInventoryObjectsDBCombo.Create(Self);
  // Give it a parent
  cbInventoryObjects.Parent := gbForceProperties;
  // Anchor it to the groupboxe's borders
  cbInventoryObjects.AnchorParallel(akLeft, 8, gbForceProperties);
  cbInventoryObjects.Anchors := cbInventoryObjects.Anchors - [akRight];
  cbInventoryObjects.Width := (gbForceProperties.ClientWidth - 3 * 8) div 2;
  // Insert it into the SpinEdit chains
  cbInventoryObjects.AnchorToNeighbour(akTop, 5, seInventoryObjects);
  seSpriteColorScheme.AnchorToNeighbour(akTop, 5, cbInventoryObjects);
  // Arrange the tab order
  cbInventoryObjects.TabOrder := seInventoryObjects.TabOrder + 1;
  // Assign the event handler
  cbInventoryObjects.OnChange := @cbInventoryObjectsChange;
end;

procedure TFMain.AddFollowValueCombo;
begin
  // Create the combo
  cbFollowValue := TForceNameDBCombo.Create(Self);
  // Give it a parent
  cbFollowValue.Parent := gbForceProperties;
  // Anchor it to the groupboxe's borders
  cbFollowValue.AnchorParallel(akRight, 8, gbForceProperties);
  cbFollowValue.Anchors := cbFollowValue.Anchors - [akLeft];
  cbFollowValue.Width := (gbForceProperties.ClientWidth - 3 * 8) div 2;
  // Insert it into the SpinEdit chains
  cbFollowValue.AnchorToNeighbour(akTop, 5, seHitPointsQuantity);
  // Arrange the tab order
  cbFollowValue.TabOrder := seFollowValue.TabOrder + 1;
  // Assign the event handler
  cbFollowValue.OnChange := @cbFollowValueChange;
end;

procedure TFMain.UpdateLocation;
var
  SpinEditLocation, ComboLocation: TPoint;
begin
  SpinEditLocation := Point(seLocationX.Value, seLocationY.Value);
  ComboLocation := cbLocation.Location;
  if ComboLocation <> SpinEditLocation then
    cbLocation.Location := SpinEditLocation;
end;

procedure TFMain.UpdateDestination;
var
  SpinEditDestination, ComboDestination: TPoint;
begin
  SpinEditDestination := Point(seDestinationX.Value, seDestinationY.Value);
  ComboDestination := cbDestination.Location;
  if ComboDestination <> SpinEditDestination then
    cbDestination.Location := SpinEditDestination;
end;

procedure TFMain.UpdateInventoryObjects;
var
  SpinEditInventoryObjects, ComboInventoryObjects: Word;
begin
  SpinEditInventoryObjects := seInventoryObjects.Value;
  ComboInventoryObjects := cbInventoryObjects.InventoryObjectsAsWord;
  if ComboInventoryObjects <> SpinEditInventoryObjects then
    cbInventoryObjects.InventoryObjectsAsWord := SpinEditInventoryObjects;
end;

procedure TFMain.UpdateMapIcon;
var
  SpinEditMapIconID, ComboMapIconID: Integer;
begin
  SpinEditMapIconID := seMapIcon.Value;
  ComboMapIconID := cbMapIcon.MapIconGameID;
  if ComboMapIconID <> SpinEditMapIconID then
    cbMapIcon.MapIconGameID := SpinEditMapIconID;
end;

procedure TFMain.UpdateFollowValue;
var
  SpinEditFollowValue, ComboFollowValue: Integer;
begin
  SpinEditFollowValue := seFollowValue.Value;
  ComboFollowValue := cbFollowValue.ItemIndex;
  if ComboFollowValue <> SpinEditFollowValue then
    cbFollowValue.ItemIndex := SpinEditFollowValue;
end;

function TFMain.GetControlsEnabled: Boolean;
begin
  Result := tvForces.Enabled;
end;

procedure TFMain.Open;
const
  PlatformIDs: array[TPlatform] of DWord = (
    $19144E35, // PC VGA
    $1B2E4A61, // PC EGA
    $0001A7C8, // ST
    $0002423A, // Amiga
    $8AAD0200  // IIGS
  );
var
  Node: TTreeNode;
  I: Integer;
  fsInput: TFileStream;
  NodeTitle: String;
  TestForce: TRawForce;
  TestID, SwappedTestID: DWord;
function FindFilePlatform(): TPlatform;
  var
    P: TPlatform;
    E: TEndian;
  begin
    Result := plPCVGA;
    for P := Low(TPlatform) to High(TPlatform) do
    begin
      E := PlatformEndian[P];
      case E of
      endLittle:
        begin
          if TestID = PlatformIDs[P] then
            Result := P;
        end;
      endBig:
        begin
          if SwappedTestID = PlatformIDs[P] then
            Result := P;
        end;
      end;
    end;
  end;
begin
  ControlsEnabled := False;
  FEndian := endLittle;
  try
    fsInput := TFileStream.Create(FFileName, fmOpenRead);
    // Try to recognise the endian by reading one force
    {$HINTS OFF}
    fsInput.ReadBuffer(TestForce, SizeOf(TestForce));
    {$HINTS ON}
    TestID := TestForce.CharacterIdentifier;
    SwappedTestID := SwapEndian(TestID);
    //ShowMessage(IntToHex(TestId, 8) + LineEnding + IntToHex(SwappedTestID, 8));
    FPlatform := FindFilePlatform();
    FEndian := PlatformEndian[FPlatform];
    {ShowMessage(IntToStr(QWord(FPlatform)) + LineEnding +
      IntToStr(QWord(FEndian)));}

    fsInput.Position := 0;
    case FEndian of
    endLittle:
      fsInput.ReadBuffer(FLittleEndianForces, SizeOf(FLittleEndianForces));
    endBig:
      begin
        fsInput.ReadBuffer(FBigEndianForces, SizeOf(FBigEndianForces));
        FLittleEndianForces := FBigEndianForces;
      end;
    end;
  finally
    fsInput.Free;
  end;
  tvForces.Items.Clear;
  Node := nil;
  for I := Low(FLittleEndianForces) to High(FLittleEndianForces) do
  begin
    {NodeTitle := IntToHex(I * SizeOf(TBigEndianForce), 4);
    NodeTitle += ' - ' + ForceNames[I];}
    NodeTitle := PadInt(I, 3) + txtDash + ForceNames[I];
    Node := tvForces.Items.Add(Node, NodeTitle);
  end;
  tvForces.Items[0].Selected := True;
  ControlsEnabled := True;
end;

procedure TFMain.Save;
var
  fsOutput: TFileStream;
  Mode: Word;
begin
  ControlsEnabled := False;
  try
    if FileExists(FFileName) then
      Mode := fmOpenWrite
    else
      Mode := fmCreate;
    fsOutput := TFileStream.Create(FFileName, Mode);
    fsOutput.Position := 0;
    case FEndian of
      endLittle:
        fsOutput.WriteBuffer(FLittleEndianForces, SizeOf(FLittleEndianForces));
      endBig:
      begin
        FBigEndianForces := FLittleEndianForces;
        fsOutput.WriteBuffer(FBigEndianForces, SizeOf(FBigEndianForces));
      end;
    end;
    ShowMessageFmt(txtChangesSaved, [FFileName]);
    ControlsEnabled := True;
  finally
    fsOutput.Free;
  end;
end;

procedure TFMain.SetControlsEnabled(Enable: Boolean);
begin
  tvForces.Enabled := Enable;
  gbForceProperties.Enabled := Enable;
  lbCount.Enabled := Enable;
  seCount.Enabled := Enable;
  lbCountAlive.Enabled := Enable;
  seCountAlive.Enabled := Enable;
  lbMoraleTotal.Enabled := Enable;
  seMoraleTotal.Enabled := Enable;
  lbMoraleQuantity.Enabled := Enable;
  seMoraleQuantity.Enabled := Enable;
end;

end.
