program GenCityDBUnit;
uses
  Classes, StrUtils;
const
  DBFileName: String = 'WIMECityNameDB.dat';
  UnitFileName: String = 'wimecitynamedb.pas';
var
  Lines: TStringList;
  Line: TStringList;
  L: Integer;
  sLine: String;
  sCityName, sOutputCityName, sX, sY: String;
  UnitFile: TextFile;
  NoneIndex: Integer;
begin
  Lines := TStringList.Create;
  Line := TStringList.Create;
  Line.Delimiter := #9;
  Line.StrictDelimiter := True;
  try
    Lines.LoadFromFile(DBFileName);
    Lines.Sort;
    // Dirty hack: Move the 'NONE' city entry to the front
    NoneIndex := Lines.IndexOf('NONE'#9'0'#9'0'#9);
    Lines.Move(NoneIndex, 0);
    try
      Assign(UnitFile, UnitFileName);
      Rewrite(UnitFile);
      WriteLn(UnitFile, 'unit WIMECityNameDB;');
      WriteLn(UnitFile, 'interface');
      WriteLn(UnitFile, 'uses');
      WriteLn(UnitFile, '    WIMECityNameDBObj;');
      WriteLn(UnitFile, 'var');
      WriteLn(UnitFile, '    CityNameDB: TCityNameDB;');
      WriteLn(UnitFile, 'implementation');
      WriteLn(UnitFile, 'initialization');
      WriteLn(UnitFile, '    {$WARNINGS OFF}');
      WriteLn(UnitFile, '    CityNameDB := TCityNameDB.Create;');
      WriteLn(UnitFile, '    {$WARNINGS ON}');
      for L := 0 to Lines.Count - 1 do
      begin
        sLine := Lines.Strings[L];
        Line.DelimitedText := sLine;
        sCityName := Line.Strings[0];
        sX := Line.Strings[1];
        sY := Line.Strings[2];
        sOutputCityName := '''' + ReplaceStr(sCityName, '''', '''''') + '''';
        WriteLn(UnitFile, '    CityNameDB.Add(',
          sOutputCityName, ', ', sX, ', ', sY,
          ');'
        );
      end;
      WriteLn(UnitFile, 'finalization');
      WriteLn(UnitFile, '    CityNameDB.Free;');
      WriteLn(UnitFile, 'end.');
    finally
      Close(UnitFile);
    end;
  finally
    Line.Free;
    Lines.Free;
  end;
end.

