unit WIMEMapIconDBObj;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, BufDataset, db, variants, ComboEx, WIMEDM;

type
  TMapIcon = record
    IconName: String;
    GameID: Integer;
  end;

  { TMapIconNameDB }

  TMapIconNameDB = class(TBufDataset)
  private
    function GetMapIconGameIDByName(MapIconName: String): Integer;
    function GetMapIconNameByGameID(MapIconGameID: Integer): String;
    function GetMapIcons(Index: Integer): TMapIcon;
    function GetMapIconNames(Index: Integer): String;
    function IndexOf(MapIconName: String): Integer;
  public
    constructor Create(AOwner: TComponent = nil); override;
    destructor Destroy; override;
    procedure Add(IconName: String; GameID: Integer);
    property MapIcons[Index: Integer]: TMapIcon read GetMapIcons;
    property MapIconNames[Index: Integer]: String read GetMapIconNames;
    property MapIconGameIDByName[MapIconName: String]: Integer
      read GetMapIconGameIDByName;
    property MapIconNameByGameID[MapIconGameID: Integer]: String
      read GetMapIconNameByGameID;
  published
  end;

  { TMapIconNameDBCombo }

  TMapIconNameDBCombo = class(TComboBoxEx)
  private
    function GetMapIconGameID: Integer;
    function GetMapIconGameIDByName(MapIconName: String): Integer;
    function GetMapIconNameByGameID(MapIconGameID: Integer): String;
    function GetMapIconNames(Index: Integer): String;
    function GetMapIcons(Index: Integer): TMapIcon;
    procedure SetMapIconGameID(AValue: Integer);
    function IndexOf(MapIconName: String): Integer;
  public
    constructor Create(AOwner: TComponent = nil); override;
    property MapIcons[Index: Integer]: TMapIcon read GetMapIcons;
    property MapIconNames[Index: Integer]: String read GetMapIconNames;
    property MapIconGameIDByName[MapIconName: String]: Integer
      read GetMapIconGameIDByName;
    property MapIconNameByGameID[MapIconGameID: Integer]: String
      read GetMapIconNameByGameID;
    property MapIconGameID: Integer read GetMapIconGameID
      write SetMapIconGameID;
  end;

const
  DefaultMapIconName = 'NONE';
  DefaultGameID = -1;
  DefaultMapIcon: TMapIcon = (
    IconName: DefaultMapIconName;
    GameId: DefaultGameID
  );


implementation

uses
  WIMEMapIconDB;

{ TMapIconNameDBCombo }

function TMapIconNameDBCombo.GetMapIconGameID: Integer;
var
  MapIconName: String;
begin
  MapIconName := MapIconNames[ItemIndex];
  Result := MapIconNameDB.MapIconGameIDByName[MapIconName];
end;

function TMapIconNameDBCombo.GetMapIconGameIDByName(MapIconName: String
  ): Integer;
begin
  Result := MapIconNameDB.MapIconGameIDByName[MapIconName];
end;

function TMapIconNameDBCombo.GetMapIconNameByGameID(MapIconGameID: Integer
  ): String;
begin
  Result := MapIconNameDB.MapIconNameByGameID[MapIconGameID];
end;

function TMapIconNameDBCombo.GetMapIconNames(Index: Integer): String;
begin
  Result := MapIconNameDB.MapIconNames[Index];
end;

function TMapIconNameDBCombo.GetMapIcons(Index: Integer): TMapIcon;
begin
  Result := MapIconNameDB.MapIcons[Index];
end;

procedure TMapIconNameDBCombo.SetMapIconGameID(AValue: Integer);
var
  CurrentGameID: Integer;
  NewMapIconName: String;
  NewItemIndex: Integer;
begin
  CurrentGameID := GetMapIconGameID;
  if CurrentGameID = AValue then Exit;
  NewMapIconName := MapIconNameByGameID[AValue];
  //Text := NewMapIconName;
  NewItemIndex := IndexOf(NewMapIconName);
  SetItemIndex(NewItemIndex);
end;

function TMapIconNameDBCombo.IndexOf(MapIconName: String): Integer;
begin
  Result := MapIconNameDB.IndexOf(MapIconName);
end;

constructor TMapIconNameDBCombo.Create(AOwner: TComponent);
var
  I: Integer;
  MapIcon: TMapIcon;
begin
  inherited Create(AOwner);
  Images := DM.ilMapIcons;
  // Load the MapIcons along with their names
  for I := 0 to MapIconNameDB.RecordCount - 1 do
  begin
    MapIcon := MapIcons[I];
    Add(MapIcon.IconName, -1, I);
  end;
  ItemIndex := 0;
end;

{ TMapIconNameDB }

function TMapIconNameDB.GetMapIconGameIDByName(MapIconName: String): Integer;
var
  BM: TBytes;
  ValArray: Variant;
begin
  BM := Bookmark;
  ValArray := VarArrayOf([MapIconName]);
  if Locate('iconname', ValArray, []) then
  begin
    Result := FieldByName('gameid').AsInteger;
  end
  else
    Result := DefaultGameID;
  Bookmark := BM;
end;

function TMapIconNameDB.GetMapIconNameByGameID(MapIconGameID: Integer): String;
var
  BM: TBytes;
  ValArray: Variant;
begin
  BM := Bookmark;
  ValArray := VarArrayOf([MapIconGameID]);
  if Locate('gameid', ValArray, []) then
  begin
    Result := FieldByName('iconname').AsString;
  end
  else
    Result := DefaultMapIconName;
  Bookmark := BM;
end;

function TMapIconNameDB.GetMapIcons(Index: Integer): TMapIcon;
var
  BM: TBytes;
  ID: Integer;
  ValArray: Variant;
begin
  BM := Bookmark;
  ID := Index + 1;
  ValArray := VarArrayOf([ID]);
  if Locate('id', ValArray, []) then
  begin
    Result.IconName := FieldByName('iconname').AsString;
    Result.GameID := FieldByName('gameid').AsInteger;
  end
  else
    Result := DefaultMapIcon;
  Bookmark := BM;
end;

function TMapIconNameDB.GetMapIconNames(Index: Integer): String;
var
  BM: TBytes;
  ID: Integer;
  ValArray: Variant;
begin
  BM := Bookmark;
  ID := Index + 1;
  ValArray := VarArrayOf([ID]);
  if Locate('id', ValArray, []) then
  begin
    Result := FieldByName('iconname').AsString;
  end
  else
    Result := DefaultMapIconName;
  Bookmark := BM;
end;

function TMapIconNameDB.IndexOf(MapIconName: String): Integer;
var
  BM: TBytes;
  ValArray: Variant;
begin
  BM := Bookmark;
  ValArray := VarArrayOf([MapIconName]);
  if Locate('iconname', ValArray, []) then
  begin
    Result := FieldByName('id').AsInteger - 1;
  end
  else
    Result := DefaultGameID;
  Bookmark := BM;
end;

constructor TMapIconNameDB.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FieldDefs.Add('id', ftAutoInc);
  FieldDefs.Add('iconname', ftString, 255);
  FieldDefs.Add('gameid', ftInteger);
  CreateDataset;
  Open;
end;

destructor TMapIconNameDB.Destroy;
begin
  inherited Destroy;
end;

procedure TMapIconNameDB.Add(IconName: String; GameID: Integer);
begin
  Append;
  FieldByName('iconname').AsString := IconName;
  FieldByName('gameid').AsInteger := GameID;
  Post;
end;

end.

