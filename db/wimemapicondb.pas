unit WIMEMapIconDB;

{$mode objfpc}{$H+}

interface
uses
  WIMEMapIconDBObj;
var
  MapIconNameDB: TMapIconNameDB;
resourcestring
  miAragorn = 'Aragorn';
  miBoromir = 'Boromir';
  miHobbitOrDwarf = 'hobbit/dwarf';
  miWizard = 'wizard';
  miGollum = 'Gollum';
  miMinasTirithInfantry1 = 'Minas Tirith infantry 1';
  miMinasTirithInfantry2 = 'Minas Tirith infantry 2';
  miDolAmrothCavalry = 'Dol Amroth cavalry';
  miDolAmrothInfantry = 'Dol Amroth infantry';
  miGondorInfantry1 = 'Gondor infantry 1';
  miGondorInfantry2 = 'Gondor infantry 2';
  miGondorLtInfantry = 'Gondor lt. infantry';
  miRohanCavalry = 'Rohan cavalry';
  miRohanLtInfantry = 'Rohan lt. infantry';
  miDaleInfantry = 'Dale infantry';
  miDaleLtInfantry = 'Dale lt. infantry';
  miEreborDwares = 'Erebor dwarves';
  miIronHillsDwarves = 'Iron Hills dwarves';
  miElf = 'elf';
  miNazgulLord = 'Nazgûl Lord';
  miNazgul = 'Nazgûl';
  miSarumanOrcs = 'Saruman orcs';
  miDunlendings = 'Dunlendings';
  miEasterlingsInfantry = 'Easterlings infantry';
  miEasterlingsLtInfantry = 'Easterlings lt. infantry';
  miHaradInfantry = 'Harad infantry';
  miHaradLtInfantry = 'Harad lt. infantry';
  miMordorTrolls = 'Mordor trolls';
  miMordorOrcs = 'Mordor orcs';
  miUmbarCorsairs = 'Umbar Corsairs';

implementation
initialization
  {$WARNINGS OFF}
  MapIconNameDB := TMapIconNameDB.Create;
  {$WARNINGS ON}
  MapIconNameDB.Add(miAragorn, 0);
  MapIconNameDB.Add(miBoromir, 1);
  MapIconNameDB.Add(miHobbitOrDwarf, 2);
  MapIconNameDB.Add(miWizard, 3);
  MapIconNameDB.Add(miGollum, 4);
  MapIconNameDB.Add(miMinasTirithInfantry1, 5);
  MapIconNameDB.Add(miMinasTirithInfantry2, 16);
  MapIconNameDB.Add(miDolAmrothCavalry, 17);
  MapIconNameDB.Add(miDolAmrothInfantry, 18);
  MapIconNameDB.Add(miGondorInfantry1, 19);
  MapIconNameDB.Add(miGondorInfantry2, 20);
  MapIconNameDB.Add(miGondorLtInfantry, 21);
  MapIconNameDB.Add(miRohanCavalry, 32);
  MapIconNameDB.Add(miRohanLtInfantry, 33);
  MapIconNameDB.Add(miDaleInfantry, 34);
  MapIconNameDB.Add(miDaleLtInfantry, 35);
  MapIconNameDB.Add(miEreborDwares, 36);
  MapIconNameDB.Add(miIronHillsDwarves, 37);
  MapIconNameDB.Add(miElf, 48);
  MapIconNameDB.Add(miNazgulLord, 49);
  MapIconNameDB.Add(miNazgul, 50);
  MapIconNameDB.Add(miSarumanOrcs, 51);
  MapIconNameDB.Add(miDunlendings, 52);
  MapIconNameDB.Add(miEasterlingsInfantry, 53);
  MapIconNameDB.Add(miEasterlingsLtInfantry, 64);
  MapIconNameDB.Add(miHaradInfantry, 65);
  MapIconNameDB.Add(miHaradLtInfantry, 66);
  MapIconNameDB.Add(miMordorTrolls, 67);
  MapIconNameDB.Add(miMordorOrcs, 68);
  MapIconNameDB.Add(miUmbarCorsairs, 69);
finalization
  MapIconNameDB.Free;
end.
