unit WIMEInventoryObjectDB;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TInventoryObject = (
    ioTheRing,
    ioDwarwenRing,
    ioGnarledStaff,
    ioAncientSword,
    ioElvenBlade,
    ioDwarvenHammer,
    ioMithrilMail,
    ioPalantir,
    ioElvenCloak,
    ioBluePotion,
    ioBlackFlask,
    ioGlowingVial,
    ioRedArrow,
    ioCoilOfRope,
    ioSceptre,
    ioSilverOrb
  );

  TInventoryObjects = set of TInventoryObject;

resourcestring
  ionTheRing = 'the Ring';
  ionADwarvenRing = 'a Dwarven Ring';
  ionAGnarledStaff = 'a Gnarled staff';
  ionAnAncientSword = 'an Ancient sword';
  ionAnElvenBlade = 'an Elven blade';
  ionADwarvenHammer = 'a Dwarven hammer';
  ionMitrilMail = 'Mithril mail';
  ionAPalantir = 'a Palantír';
  ionAnElvenCloak = 'an Elven cloak';
  ionABluePotion = 'a blue potion';
  ionABlackFlask = 'a black flask';
  ionAGlowingVial = 'a glowing vial';
  ionTheRedArrow = 'the Red Arrow';
  ionACoilOfRope = 'a coil of rope';
  ionASceptre = 'a sceptre';
  ionASilverOrb = 'a silver orb';

const
  InventoryObjectsDB: array[TInventoryObject] of String = (
    ionTheRing,
    ionADwarvenRing,
    ionAGnarledStaff,
    ionAnAncientSword,
    ionAnElvenBlade,
    ionADwarvenHammer,
    ionMitrilMail,
    ionAPalantir,
    ionAnElvenCloak,
    ionABluePotion,
    ionABlackFlask,
    ionAGlowingVial,
    ionTheRedArrow,
    ionACoilOfRope,
    ionASceptre,
    ionASilverOrb
  );

implementation

end.

