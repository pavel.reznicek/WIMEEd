unit WIMEForceNameDB;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

resourcestring
  fnLastLocation = 'nobody (last location)';
  fnGandalfTheGrey = 'Gandalf the Grey';
  fnGandalfTheWhite = 'Gandalf the White';
  fnElrond = 'Elrond';
  fnGlorfindel = 'Glorfindel';
  fnAragorn = 'Aragorn';
  fnBoromir = 'Boromir';
  fnFrodo = 'Frodo';
  fnSam = 'Sam';
  fnPippin = 'Pippin';
  fnMerry = 'Merry';
  fnLegolas = 'Legolas';
  fnGimli = 'Gimli';
  fnEomer = 'Éomer';
  fnCavalry = 'cavalry';
  fnFaramir = 'Faramir';
  fnRangers = 'Rangers';
  fnDain = 'Dáin';
  fnDwarves = 'dwarves';
  fnBrand = 'Brand';
  fnInfantry = 'infantry';
  fnLtInfantry = 'lt. infantry';
  fnThranduil = 'Thranduil';
  fnElves = 'elves';
  fnCeleborn = 'Celeborn';
  fnTheoden = 'Théoden';
  fnTheodred = 'Théodred';
  fnErkenbrand = 'Erkenbrand';
  fnDenethor = 'Denethor';
  fnTowerGuards = 'tower guards';
  fnDernhelm = 'Dernhelm';
  fnElfhelm = 'Elfhelm';
  fnGrimbold = 'Grimbold';
  fnDunhere = 'Dúnhere';
  fnImrahil = 'Imrahil';
  fnKnights = 'knights';
  fnMenAtArms = 'men at arms';
  fnForlong = 'Forlong';
  fnDervorin = 'Dervorin';
  fnDuinhir = 'Duinhir';
  fnGolasgil = 'Golasgil';
  fnHirluin = 'Hirluin';
  fnElvenGuards = 'elven guards';
  fnEnts = 'ents';
  fnHuorns = 'huorns';
  fnGollum = 'Gollum';
  fnTheNazgulLord = 'the Nazgûl Lord';
  fnThe2ndNazgul = 'the 2nd Nazgûl';
  fnThe3rdNazgul = 'the 3rd Nazgûl';
  fnThe4thNazgul = 'the 4th Nazgûl';
  fnThe5thNazgul = 'the 5th Nazgûl';
  fnThe6thNazgul = 'the 6th Nazgûl';
  fnThe7thNazgul = 'the 7th Nazgûl';
  fnThe8thNazgul = 'the 8th Nazgûl';
  fnThe9thNazgul = 'the 9th Nazgûl';
  fnHandOrcs = 'hand orcs';
  fnUrukHai = 'uruk-hai';
  fnDunlendings = 'Dunlendings';
  fnBalchoth = 'Balchoth';
  fnWainriders = 'Wainriders';
  fnEasterlings = 'Easterlings';
  fnGothmog = 'Gothmog';
  fnOrcs = 'orcs';
  fnTrolls = 'trolls';
  fnSouthrons = 'Southrons';
  fnVariags = 'Variags';
  fnHaradrim = 'Haradrim';
  fnHalfTrolls = 'halftrolls';
  fnCorsairs = 'Corsairs';
  fnSauron = 'Sauron';
  fnShelob = 'Shelob';
  fnCirdan = 'Círdan';
  fnTomBombadil = 'Tom Bombadil';
  fnBilbo = 'Bilbo';
  fnGaladriel = 'Galadriel';
  fnTreebeard = 'Treebeard';
  fnRadagast = 'Radagast';
  fnARanger = 'a Ranger';
  fnRohirrimScouts = 'Rohirrim scouts';
  fnADwarf = 'a Dwarf';
  fnAHobbit = 'a Hobbit';
  fnAnElf = 'an elf';
  fnAWanderer = 'a wanderer';
  fnATraveller = 'a traveler';
  fnAnOldMan = 'an old man';
  fnSaruman = 'Saruman';
  fnATroll = 'a troll';
  fnAWight = 'a wight';
  fnDeadMen = 'Dead Men';
  fnBandits = 'bandits';
  fnABalrog = 'a balrog';
  fnWolves = 'wolves';
  fnSpiders = 'spiders';
  fnASpider = 'a spider';

const
  ForceCount = 196;
  ForceNames: array[0..ForceCount - 1] of String = (
    fnLastLocation,
    fnGandalfTheGrey,
    fnGandalfTheWhite,
    fnElrond,
    fnGlorfindel,
    fnAragorn,
    fnBoromir,
    fnFrodo,
    fnSam,
    fnPippin,
    fnMerry,
    fnLegolas,
    fnGimli,
    fnEomer,
    fnCavalry,
    fnFaramir,
    fnRangers,
    fnDain,
    fnDwarves,
    fnBrand,
    fnInfantry,
    fnLtInfantry,
    fnThranduil,
    fnElves,
    fnCeleborn,
    fnElves,
    fnTheoden,
    fnCavalry,
    fnTheodred,
    fnCavalry,
    fnErkenbrand,
    fnCavalry,
    fnDenethor,
    fnTowerGuards,
    fnInfantry,
    fnDernhelm,
    fnElfhelm,
    fnCavalry,
    fnGrimbold,
    fnCavalry,
    fnDunhere,
    fnCavalry,
    fnImrahil,
    fnKnights,
    fnMenAtArms,
    fnForlong,
    fnMenAtArms,
    fnDervorin,
    fnInfantry,
    fnDuinhir,
    fnInfantry,
    fnGolasgil,
    fnInfantry,
    fnLtInfantry,
    fnHirluin,
    fnInfantry,
    fnElvenGuards,
    fnRangers,
    fnDwarves,
    fnElves,
    fnElves,
    fnElves,
    fnElves,
    fnElves,
    fnElves,
    fnEnts,
    fnHuorns,
    fnLtInfantry,
    fnLtInfantry,
    fnInfantry,
    fnInfantry,
    fnInfantry,
    fnInfantry,
    fnInfantry,
    fnInfantry,
    fnInfantry,
    fnInfantry,
    fnInfantry,
    fnGollum,
    fnTheNazgulLord,
    fnThe2ndNazgul,
    fnThe3rdNazgul,
    fnThe4thNazgul,
    fnThe5thNazgul,
    fnThe6thNazgul,
    fnThe7thNazgul,
    fnThe8thNazgul,
    fnThe9thNazgul,
    fnHandOrcs,
    fnHandOrcs,
    fnHandOrcs,
    fnUrukHai,
    fnDunlendings,
    fnHandOrcs,
    fnUrukHai,
    fnDunlendings,
    fnHandOrcs,
    fnBalchoth,
    fnWainriders,
    fnBalchoth,
    fnEasterlings,
    fnWainriders,
    fnWainriders,
    fnGothmog,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnTrolls,
    fnOrcs,
    fnTrolls,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnTrolls,
    fnOrcs,
    fnOrcs,
    fnTrolls,
    fnOrcs,
    fnOrcs,
    fnTrolls,
    fnOrcs,
    fnTrolls,
    fnOrcs,
    fnOrcs,
    fnSouthrons,
    fnSouthrons,
    fnHaradrim,
    fnSouthrons,
    fnVariags,
    fnHaradrim,
    fnHalfTrolls,
    fnSouthrons,
    fnVariags,
    fnSouthrons,
    fnCorsairs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnTrolls,
    fnSauron,
    fnShelob,
    fnCirdan,
    fnTomBombadil,
    fnBilbo,
    fnGaladriel,
    fnTreebeard,
    fnRadagast,
    fnARanger,
    fnRohirrimScouts,
    fnADwarf,
    fnAHobbit,
    fnAnElf,
    fnAWanderer,
    fnATraveller,
    fnAnOldMan,
    fnSaruman,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnOrcs,
    fnTrolls,
    fnTrolls,
    fnTrolls,
    fnATroll,
    fnAWight,
    fnDeadMen,
    fnDunlendings,
    fnSouthrons,
    fnBandits,
    fnOrcs,
    fnABalrog,
    fnABalrog,
    fnABalrog,
    fnABalrog,
    fnABalrog,
    fnWolves,
    fnWolves,
    fnWolves,
    fnWolves,
    fnSpiders,
    fnASpider,
    fnSpiders,
    fnASpider
  );

implementation

end.

