unit WIMEInventoryObjectDBObj;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Types, StdCtrls, LCLType, LCLIntf, Themes, Graphics,
  GraphUtil, ComboEx, ImgList, WIMEDM, WIMEInventoryObjectDB;

type

  { TInventoryObjectsDBCombo }

  TInventoryObjectsDBCombo = class(TCheckComboBox)
  private
    function GetInventoryObjects: TInventoryObjects;
    function GetInventoryObjectsAsWord: Word;
    procedure SetInventoryObjects(AValue: TInventoryObjects);
    procedure SetInventoryObjectsAsWord(AValue: Word);
  public
    constructor Create(AOwner: TComponent); override;
    procedure DrawItem(Index: Integer; ARect: TRect; ODState: TOwnerDrawState);
      override;
  published
    property InventoryObjects: TInventoryObjects read GetInventoryObjects
      write SetInventoryObjects;
    property InventoryObjectsAsWord: Word read GetInventoryObjectsAsWord
      write SetInventoryObjectsAsWord;
  end;

implementation

{ TInventoryObjectsDBCombo }

function TInventoryObjectsDBCombo.GetInventoryObjects: TInventoryObjects;
var
  I: TInventoryObject;
begin
  Result := [];
  for I := Low(TInventoryObject) to High(TInventoryObject) do
  begin
    if Checked[Integer(I)] then Result += [I];
  end;
end;

function TInventoryObjectsDBCombo.GetInventoryObjectsAsWord: Word;
var
  RawInventoryObjects: TInventoryObjects;
  wInventoryObjects: Word absolute RawInventoryObjects;
begin
  RawInventoryObjects := InventoryObjects;
  Result := wInventoryObjects;
end;

procedure TInventoryObjectsDBCombo.SetInventoryObjects(AValue: TInventoryObjects
  );
var
  I: TInventoryObject;
begin
  for I := Low(TInventoryObject) to High(TInventoryObject) do
  begin
    Checked[Integer(I)] := I in AValue;
  end;
end;

procedure TInventoryObjectsDBCombo.SetInventoryObjectsAsWord(AValue: Word);
var
  ioValue: TInventoryObjects absolute AValue;
begin
  InventoryObjects := ioValue;
end;

constructor TInventoryObjectsDBCombo.Create(AOwner: TComponent);
var
  I: TInventoryObject;
begin
  inherited Create(AOwner);
  for I := Low(TInventoryObject) to High(TInventoryObject) do
  begin
    AddItem(InventoryObjectsDB[I], cbUnchecked);
  end;
  ItemIndex := 0;
end;

procedure TInventoryObjectsDBCombo.DrawItem(Index: Integer; ARect: TRect;
  ODState: TOwnerDrawState);
const                  { Enabled, State, Highlighted }
  caCheckThemes: array [Boolean, TCheckBoxState, Boolean] of TThemedButton =
    { normal, highlighted }
    (((tbCheckBoxUncheckedDisabled, tbCheckBoxUncheckedDisabled),
      { disabled, unchecked }
      (tbCheckBoxCheckedDisabled, tbCheckBoxCheckedDisabled),
      { disabled, checked }
      (tbCheckBoxMixedDisabled, tbCheckBoxMixedDisabled)),
      { disabled, greyed }
     ((tbCheckBoxUncheckedNormal, tbCheckBoxUncheckedHot),
      { enabled, unchecked }
      (tbCheckBoxCheckedNormal, tbCheckBoxCheckedHot),
      { enabled, checked }
      (tbCheckBoxMixedNormal, tbCheckBoxMixedHot)));
      { enabled, greyed }
  cCheckIndent: SmallInt = 2;
  cImageIndent: SmallInt = 5;
  cTextIndent: SmallInt = 0;
var
  aDetail: TThemedElementDetails;
  aDropped: Boolean;
  aEnabled: Boolean;
  aFlags: Cardinal;
  { combo has edit-like line edit in csDropDownList (Win) and is closed
  (not DroppedDown) }
  aFocusedEditableMainItemNoDD: Boolean;
  aGray: Byte;
  anyRect: TRect;
  aState: TCheckBoxState;
  ItemState: TCheckComboItemState;
  aImageSize: TSize;
begin  { do not call inherited ! }
  ItemState:=TCheckComboItemState(Items.Objects[Index]);
  if not (ItemState is TCheckComboItemState) then
    QueueCheckItemStates;
  aDropped := DroppedDown;
  if aDropped and FRejectDropDown then
    begin
      DroppedDown := False;
      exit;  { Exit! }
    end;
  aEnabled := IsEnabled;
  if not (csDesigning in ComponentState) then
    aEnabled := (aEnabled and ItemState.Enabled);
  {$IF DEFINED(LCLWin32) or DEFINED(LCLWin64)}
  aFocusedEditableMainItemNoDD := (Focused and (ARect.Left>0) and not aDropped);
  {$ELSE}
  aFocusedEditableMainItemNoDD := False;
  {$ENDIF}
  if (ARect.Left = 0) or aFocusedEditableMainItemNoDD then
  begin
    if odSelected in ODState then
    begin
      if not aEnabled then
        begin
          aGray:=ColorToGray(Canvas.Brush.Color);
          Canvas.Brush.Color:=RGBToColor(aGray, aGray, aGray);
        end;
    end
    else
      Canvas.Brush.Color:=clWindow;
    Canvas.Brush.Style:=bsSolid;
    Canvas.FillRect(ARect);
  end;
  if not (csDesigning in ComponentState) then
    aState := ItemState.State
  else
    aState := cbUnchecked;
  aDetail := ThemeServices.GetElementDetails(
    caCheckThemes[aEnabled, aState, not aDropped and FCheckHighlight]
  );
  if FNeedMeasure then
  begin
    FCheckSize := ThemeServices.GetDetailSize(aDetail);
    FTextHeight := Canvas.TextHeight('ŠjÁŮÇ');
    if not aDropped then
    begin
      if not FRightToLeft then
      begin
        FHiLiteLeft := -1;
        FHiLiteRight := ARect.Right;
      end else
      begin
        FHiLiteLeft := ARect.Left;
        FHiLiteRight := ARect.Right;
      end;
      FNeedMeasure := False;
    end;
  end;
  // Draw the checkbox
  if not FRightToLeft then
    anyRect.Left := ARect.Left + cCheckIndent
  else
    anyRect.Left := ARect.Right - cCheckIndent - FCheckSize.cx;
  anyRect.Right := anyRect.Left + FCheckSize.cx;
  anyRect.Top := (ARect.Bottom + ARect.Top - FCheckSize.cy) div 2;
  anyRect.Bottom := anyRect.Top + FCheckSize.cy;
  ThemeServices.DrawElement(Canvas.Handle, aDetail, anyRect);
  Canvas.Brush.Style := bsClear;
  if (not (odSelected in ODState) or not aDropped) and
  not aFocusedEditableMainItemNoDD then
    Canvas.Font.Color := clWindowText
  else
    Canvas.Font.Color := clHighlightText;
  if aFocusedEditableMainItemNoDD then
  begin
    LCLIntf.SetBkColor(Canvas.Handle, ColorToRGB(clBtnFace));
    LCLIntf.DrawFocusRect(Canvas.Handle, aRect);
  end;
  // Draw the image
  aFlags := DT_END_ELLIPSIS or DT_VCENTER or DT_SINGLELINE or DT_NOPREFIX;
  LCLIntf.SetBkColor(Canvas.Handle, ColorToRGB(clNone));
  if not FRightToLeft then
  begin
    anyRect.Left := ARect.Left + cCheckIndent + FCheckSize.cx + cImageIndent;
    anyRect.Right := ARect.Right;
  end
  else
  begin
    anyRect.Right := anyRect.Left - cImageIndent;
    anyRect.Left := ARect.Left;
    aFlags := aFlags or DT_RIGHT or DT_RTLREADING;
  end;
  DM.ilInventoryObjects.DrawingStyle := dsTransparent;
  DM.ilInventoryObjects.Draw(Canvas, anyRect.Left, anyRect.Top, Index,
    not (odDisabled in ODState));
  // Draw the text
  aImageSize := Size(DM.ilInventoryObjects.Width, DM.ilInventoryObjects.Height);
  if not FRightToLeft then
  begin
    anyRect.Left := ARect.Left + cCheckIndent + FCheckSize.cx + cImageIndent +
      aImageSize.cx + cTextIndent;
    anyRect.Right := ARect.Right;
  end else
  begin
    anyRect.Right := anyRect.Left - cTextIndent;
    anyRect.Left := ARect.Left;
    aFlags := aFlags or DT_RIGHT or DT_RTLREADING;
  end;
  anyRect.Top := (ARect.Top + ARect.Bottom - FTextHeight) div 2;
  anyRect.Bottom := anyRect.Top + FTextHeight;
  ThemeServices.DrawText(Canvas, aDetail, Items[Index], anyRect, aFlags, 0);
end;


end.

