unit WIMEForceNameDBObj;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls, WIMEForceNameDB;

type

  { TForceNameDBCombo }

  TForceNameDBCombo = class(TComboBox)
  public
    constructor Create(TheOwner: TComponent); override;
  end;

resourcestring
  txtDash = '—';

implementation

{ TForceNameDBCombo }

constructor TForceNameDBCombo.Create(TheOwner: TComponent);
var
  F: Integer;
  ForceName, ForceItemFormat, ForceItem: String;
begin
  inherited Create(TheOwner);
  Style := csDropDownList;
  ForceItemFormat := '%d' + txtDash + '%s';
  for F := 0 to ForceCount - 1 do
  begin
    ForceName := ForceNames[F];
    ForceItem := Format(ForceItemFormat, [F, ForceName]);
    AddItem(ForceItem, nil);
  end;
end;

end.

