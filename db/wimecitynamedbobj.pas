unit WIMECityNameDBObj;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DB, BufDataset, Variants, StdCtrls, DefaultTranslator;

type
  TCity = record
    Name: String;
    Location: TPoint;
  end;

  { TCityNameDB }

  TCityNameDB = class(TBufDataset)
  private
    function GetCities(Index: Integer): TCity;
    function GetLocationByName(CityName: String): TPoint;
    function GetNameByLocation(Location: TPoint): String;
    function GetCityNames(Index: Integer): String;
  public
    constructor Create(AOwner: TComponent = nil); override;
    destructor Destroy; override;
    procedure Add(CityName: String; X, Y: Integer);
    property Cities[Index: Integer]: TCity read GetCities;
    property CityNames[Index: Integer]: String read GetCityNames;
    property LocationByName[CityName: String]: TPoint read GetLocationByName;
    property NameByLocation[Location: TPoint]: String read GetNameByLocation;
  published
  end;

  { TCityNameDBCombo }

  TCityNameDBCombo = class(TComboBox)
  private
    function GetCities(Index: Integer): TCity;
    function GetCityNames(Index: Integer): String;
    function GetLocation: TPoint;
    function GetLocationByName(CityName: String): TPoint;
    function GetNameByLocation(Location: TPoint): String;
    procedure SetLocation(AValue: TPoint);
  public
    constructor Create(AOwner: TComponent); override;
    property Cities[Index: Integer]: TCity read GetCities;
    property CityNames[Index: Integer]: String read GetCityNames;
    property LocationByName[CityName: String]: TPoint read GetLocationByName;
    property NameByLocation[Location: TPoint]: String read GetNameByLocation;
    property Location: TPoint read GetLocation write SetLocation;
  end;

resourcestring
  DefaultCityName = 'hinterland';
const
  DefaultLocation: TPoint = (x: -1; y: -1);
  DefaultCity: TCity = (Name: DefaultCityName; Location: (x: -1; y: -1));

implementation

uses WIMECityNameDB;

{ TCityNameDBCombo }

function TCityNameDBCombo.GetCities(Index: Integer): TCity;
begin
  Result := CityNameDB.Cities[Index];
end;

function TCityNameDBCombo.GetCityNames(Index: Integer): String;
begin
  Result := CityNameDB.CityNames[Index];
end;

function TCityNameDBCombo.GetLocation: TPoint;
begin
  Result := CityNameDB.LocationByName[Text];
end;

function TCityNameDBCombo.GetLocationByName(CityName: String): TPoint;
begin
  Result := CityNameDB.LocationByName[CityName];
end;

function TCityNameDBCombo.GetNameByLocation(Location: TPoint): String;
begin
  Result := CityNameDB.NameByLocation[Location];
end;

procedure TCityNameDBCombo.SetLocation(AValue: TPoint);
var
  CurrentCityName: String;
  NewCityName: String;
begin
  CurrentCityName := Text;
  NewCityName := NameByLocation[AValue];
  if NewCityName <> CurrentCityName then
    Text := NewCityName;
end;

constructor TCityNameDBCombo.Create(AOwner: TComponent);
var
  C: Integer;
  CityName: String;
  DefaultCityNameIndex: Integer;
  NoneIndex: Integer;
begin
  inherited Create(AOwner);
  // We want the drop down list style to be the default
  Style := csDropDownList;
  // Add the 'HINTERLAND' item for nameless locations
  AddItem(DefaultCityName, nil);
  // Loop through the city name database and add each name to the combo
  for C := 0 to CityNameDB.RecordCount - 1 do
  begin
    CityName := CityNameDB.CityNames[C];
    AddItem(CityName, nil);
  end;
  Sorted := True;
  Sorted := False;
  DefaultCityNameIndex := Items.IndexOf(DefaultCityName);
  Items.Move(DefaultCityNameIndex, 0);
  NoneIndex := Items.IndexOf(cnNone);
  Items.Move(NoneIndex, 1);
  ItemIndex := 0;
end;

{ TCityNameDB }

function TCityNameDB.GetCities(Index: Integer): TCity;
var
  BM: TBytes;
  ID: Integer;
begin
  //Save current position
  BM := Bookmark;
  ID := Index + 1;
  if Locate('id', ID, []) then
  begin
    Result.Name := FieldByName('cityname').AsString;
    Result.Location.x := FieldByName('x').AsInteger;
    Result.Location.y := FieldByName('y').AsInteger;
  end
  else
    Result := DefaultCity;
  // Restore original position
  Bookmark := BM;
end;

function TCityNameDB.GetLocationByName(CityName: String): TPoint;
var
  BM: TBytes;
begin
  //Save current position
  BM := Bookmark;
  if Locate('cityname', CityName, []) then
  begin
    Result.x := FieldByName('x').AsInteger;
    Result.y := FieldByName('y').AsInteger;
  end
  else
    Result := Point(-1, -1);
  // Restore original position
  Bookmark := BM;
end;

function TCityNameDB.GetNameByLocation(Location: TPoint): String;
var
  BM: TBytes;
  ValArray: Variant;
begin
  //Save current position
  BM := Bookmark;
  ValArray := VarArrayOf([Location.x, Location.y]);
  if Locate('x; y', ValArray, []) then
  begin
    Result := FieldByName('cityname').AsString;
  end
  else
    Result := DefaultCityName;
  // Restore original position
  Bookmark := BM;
end;

function TCityNameDB.GetCityNames(Index: Integer): String;
var
  BM: TBytes;
  ID: Integer;
  ValArray: Variant;
begin
  BM := Bookmark;
  ID := Index + 1;
  ValArray := VarArrayOf([ID]);
  if Locate('id', ValArray, []) then
  begin
    Result := FieldByName('cityname').AsString;
  end
  else
    Result := DefaultCityName;
  Bookmark := BM;
end;

constructor TCityNameDB.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FieldDefs.Add('id', ftAutoInc);
  FieldDefs.Add('cityname', ftString, 255);
  FieldDefs.Add('x', ftInteger);
  FieldDefs.Add('y', ftInteger);
  CreateDataset;
  Open;
end;

destructor TCityNameDB.Destroy;
begin
  if Active then Close;
  inherited Destroy;
end;

procedure TCityNameDB.Add(CityName: String; X, Y: Integer);
begin
  Append;
  FieldByName('cityname').AsString := CityName;
  FieldByName('x').AsInteger := X;
  FieldByName('y').AsInteger := Y;
  Post;
end;

end.

