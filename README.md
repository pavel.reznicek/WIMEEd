# WIMEEd

A small utility to edit saved game files from the War in Middle Earth game 
published in 1988 by Synergistic Software and Melbourne House.

## License
This software is distributed under the terms of the GPL v. 3.0 license.

## Disclaimer
There is no warranty to the use or misuse of this software, to the extent 
allowed by applicable law.

## Credits
Many thanks go to Aaron R. Willis, author of the 
*War in Middle Earth Game Editor,* a far more advanced editor for Windows
written in Visual Basic .NET that I also contributed to and we had a lot of fun
working together on discovering various mysteries of the War in Middle Earth
game. Aaron’s discoveries and information are also used in my WIMEEd project.

The source code and installer of the Aaron’s editor can be found 
on GitHub:

https://github.com/arwillis77/WIME-Editor

This project isn’t meant as any kind of competition to War in Middle Earth 
Game Editor. Instead, it aims to be a complement to satisfy Linux users. As a 
side effect of the FreePascal’s multiplatform nature, WIMEEd has also builds
for Windows. They can serve as a simple and compact alternative to War in Middle 
Earth Game Editor.

## Programming language
Written in FreePascal using the Lazarus IDE (see http://lazarus.freepascal.org).

## What WIMEEd supports
At the time of writing this (April 23rd 2019), WIMEEd supports changing
of all the discovered force properties as numbers. More intuitive editation
of some of them is planned.

## Various versions of WIME
War in Middle Earth was published in various versions for several platforms, 
including Apple IIGS, Commodore Amiga, Atari ST, PC EGA, and PC VGA. However, 
the savegame files (archive.dat) have a simple, common, and firm format so 
nearly the only thing that changes is the endian (order of bytes) they are 
encoded in. 
The endian can be detected automatically so you can edit just any WIME 
savegame file with WIMEEd. If you discover a version that doesn’t get decoded
properly, please let me know.

## Getting a binary release

### Installing via Ubuntu PPA

On Ubuntu based distributions, you can use the Private Package Archive (PPA).

Open the terminal and type these commands (you’ll be prompted for you password):

```bash
sudo add-apt-repository ppa:cigydd/wimeed
sudo apt-get update
```

If the PPA was successfully added you can continue with the installation:
```bash
sudo apt-get install wimeed
```

That’s all.

### Debian and RPM package files (*.deb, *.rpm)

Go to the downloads section of this repository and download the deb file 
suitable for your processor architecture. (If you don’t know what architecture 
you have just type `uname -m` in the terminal and you’ll get 
i386, i486, i586, i686, x86_64, amd64, or ia64. For the i<x>86 architectures,
download the i386 package. For x86_64, amd64, or ia64, download the amd64 or 
x86_64 package.

Who likes it graphical-only he can double-click on the downloaded package 
in the file manager of his favourite graphical environment 
(Caja, Nemo, Nautilus…) and if it works he can continue by following the 
instructions on the screen.

For those who like the comand line: Open the terminal, go to the folder where 
you downloaded the package, and install the package. You can do so in many ways,
one of them is this (providing that you downloaded to the Downloads folder:

```bash
cd ~/Downloads
sudo dpkg -i wimeed-0.1.0.6-1_amd64.deb
```

Here, the _dpkg_ program was used. You could replace the second line with this:

```bash
sudo gdebi wimeed-0.1.0.6-1_amd64.deb
```

That was just another program, called _gdebi_. Before calling gdebi, 
you have to install it first if you haven’t done so yet. If you want it more 
graphical, do this:

```bash
sudo -i gdebi-gtk wimeed-0.1.0.6-1_amd64.deb
```

Again, you’ll have to install _gdebi-gtk_ if you haven’t done so yet.

To show an example for an RPM-based distribution, the second line would look 
something like this:

```bash
yum install wimeed-0.1.0.6-x86_64.rpm
```

At the time of writing this (October 6th 2018), the RPM packages are not 
available yet but they are on the plan too. Please be patient and stay tuned.

### .tar.xz and .zip archives

Go to the Downloads section of this repository and find the binary release 
that’s suitable just for your computer. At this time, the releases are just 
the WIMEEd binary files accompanied with this README file (README.md) and the 
GNU GPL v. 3.0 (LICENSE.md) compressed with contemporary standard archivers for 
each platform. So, for Linux, the name of the archive consists of the name of 
this software, its version, operating system, and CPU architecture, in this 
order, and the extension.
So for v. 0.0, Linux, and a 64-bit CPU, you get 
WIMEEd-0.0-linux-x86_64.tar.xz
and for v. 0.0, Windows, and a 32-bit CPU, you get
WIMEEd-0.0-win32-i386.zip.

Download the appropriate file and unpack it. Common GUI archivers (Engrampa, 
7zip, WinRAR, Windows Explorer) shall do the job for you. On Linux, you can 
unpack the file with these commands (assuming you downloaded the file to your 
Downloads folder):

```bash
cd ~/Downloads
tar --xz -xvf WIMEEd-0.0-linux-x86_64.tar.xz
```

You’ll get a directory named like `WIMEEd-0.0-linux-x86_64`, containing (though 
not only) the file `wimeed`, or `wimeed.exe`, respectively. That’s the 
executable binary file, the whole program.

#### Running directly from your desktop
You can do it a dirtier but simpler way and copy/move the executable file 
directly to your desktop. Then try to double-click on the executable and if the 
main window shows up, your installation procedure succeeded.

#### Installing the binary the cleaner way
You can move the binary to a directory that’s in your PATH. Recommended location
is /usr/local/bin on Linux and C:\Program Files\WIMEEd on Windows. On that OS 
you may wish to add it to your PATH to be able to start it from the command 
line. You may need administrator privileges to make a subfolder in the 
Program Files directory and move the binary there. On both systems, you can add 
a shortcut to your desktop or to the main/Start menu by hand (right-click 
on the file and choose something as „Make a shortcut on your desktop” or 
„Send To… → Desktop (Make a shortcut)”. 

For Linux, the install commands would read:
```bash
sudo mv wimeed /usr/local/bin
sudo chown root:root /usr/local/bin/wimeed
```
The second line changes the owner to the superuser to keep things in order.


## Running the executable
You should now be able to run your installed WIMEEd binary.

On Linux, try to type:
```bash
wimeed
```
in the terminal.

For older versions, type:
```bash
WIMEEd
```

Windows users may try to click on the shortcut they made or also if they added 
the program folder to their PATH they may test it by opening the command line 
and typing:
```batch
wimeed
```

If the main window shows up, your installation procedure succeded.

## Compiling from source
You can compile (or build) the executable file yourself by downloading 
and unpacking, or cloning the source repository, and building with Lazarus. 
To clone this repository, you need to have Git installed. The actual cloning 
command reads:
```bash
git clone git@bitbucket.org:cigydd/wimeed.git
```
It will make a new subdirectory in the current directory containing all the 
sources. Start Lazarus, choose Project → Open project from the menu, find the 
WIMEEd.lpi file and confirm.

To build the project, choose Run → Build. If everything goes well, you should 
get the executable file named `WIMEEd`, or `WIMEEd.exe`, respectively. You can 
then run that file to test if the program works.

## Opening a savegame file
First, you need to copy the game files (or, strictly speaking, only the 
savegame file) to your hard drive or anywhere your computer can open it from. 
The common use of the original game files is on emulators so if you already 
copied the game files to a directory the emulator runs it from you can keep it 
there. If you value your savegame file much (e. g., if it contains a strategic 
situation you saved for a purpose) always be sure to make a backup copy first.

Then, you are prepared to open the saved game. Click on the Open button on the 
toolbar and locate the savegame file. It will most probably be called 
`archive.dat` and reside somewhere in your original WIME folder tree. To aid 
you with finding it, the default file filter of the open dialog is set to 
archive.dat only so you should see it and nothing else. If you made some 
backups (recommended extension is .dat), you can switch the filter to all .dat 
files, or to all files in general, to be able to see them.

Having found that file on your system, click on it and click on the “Open” 
button or, alternatively, double-click on the file. The dialog should close 
and the file should open.

## Editing

On the left side of the main window, you see the force list. Each node of the 
tree view shows the sedecimal offset of the force in the savegame file and the 
force’s name. You can click on the nodes to choose and see the force’s 
properties in the right part of the window.

Funny is that you can set the force member count to greater than one even for 
forces such as Frodo or Gandalf and so you can massively strengthen your chances
to win the game. It can also be used to test the strength of individual forces 
in battle. But it has a side effect that you can’t pick up objects with such a
multiplied character. You’ll always get an alert saying “Please select an active
member of the party.”

To change the numeric properties, you can type the numbers in and confirm with 
Enter, you can use the arrow buttons, or even arrow keys on your keyboard.

As you change the numbers, nothing gets saved yet.

Note that the maximal number of force members in the game equals to the maximal 
unsigned integer, nowadays called unsigned short integer or an unsigned word. 
That makes 65,535 members. No, you can’t summon any more Gandalfs ;-) On some 
platforms such as PC VGA, you’ll see negative numbers if setting the member
count above 32,767. The counts will decrease in combat but in fact the effect 
will be as if new troops were added. You can also encounter strange bugs
if setting the member counts to the maximum.

## Saving
The saving procedure is as straightfoward as the open procedure. Just click 
on the save button on the toolbar and choose the file name you’d wish to save 
your changes under. It could be the original file you opened (set as default) 
or any other file. To be able to type in another name than `archive.dat`, 
switch the filter to one of the two more common ones (`*.dat` or `*`).

## Loading the saved game in WIME
To be able to load the modified saved game file in WIME, zoom out to the world 
map clicking on the map icon until you get there, and then use the scroll icon. 
It will present you with a paper with two buttons: “Restore an old game” and 
”Save current game”. Choose to restore and zoom in to watch the effect of your 
changes. The second button comes in handy if you want to save your game state 
for the future. The previous changes you made with WIMEEd may or may not 
persist.

## Uninstallation
If you copied the executable binary file to your system, just remove it.

For Linux command line, this could look like:
```bash
sudo rm /usr/local/bin/wimeed
```
For older releases:
```bash
sudo rm /usr/local/bin/WIMEEd
```

If yout installed WIMEEd from a Linux package, just remove the package. 
On a Debian based system, it would look like:
```bash
sudo apt remove wimeed
```

## Reporting bugs
If you think you found a bug you can open an issue ticket here:
https://bitbucket.org/cigydd/wimeed/issues

Enjoy the editor!
Pavel Řezníček A. K. A. Cigydd

