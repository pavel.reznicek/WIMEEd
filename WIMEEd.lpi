<?xml version="1.0" encoding="UTF-8"?>
<CONFIG>
  <ProjectOptions>
    <Version Value="11"/>
    <PathDelim Value="\"/>
    <General>
      <SessionStorage Value="InProjectDir"/>
      <MainUnit Value="0"/>
      <Title Value="WIMEEd"/>
      <ResourceType Value="res"/>
      <UseXPManifest Value="True"/>
      <XPManifest>
        <TextName Value="Cigydd.WIMEEd.WIMEEd"/>
        <TextDesc Value="A savegame editor for the War in Middle Earth game (1988)"/>
      </XPManifest>
      <Icon Value="0"/>
    </General>
    <i18n>
      <EnableI18N Value="True"/>
      <OutDir Value="locale"/>
    </i18n>
    <VersionInfo>
      <UseVersionInfo Value="True"/>
      <MajorVersionNr Value="1"/>
      <StringTable LegalCopyright="Copyleft Pavel Řezníček 2018–19 GNU/GPL v. 3" ProductName="WIMEEd"/>
    </VersionInfo>
    <CustomData Count="13">
      <Item0 Name="lazpackager/copyright" Value="2018 Pavel Řezníček"/>
      <Item1 Name="lazpackager/deb/ppa" Value="ppa:cigydd/wimeed"/>
      <Item2 Name="lazpackager/deb/series" Value="bionic"/>
      <Item3 Name="lazpackager/deb/tpl_changelog" Value="?PACKAGE_NAME? (?FULLVERSION?) ?SERIES?; urgency=low

  * Original version ?VERSION? packaged with lazdebian

 -- ?MAINTAINER? &lt;?MAINTAINER_EMAIL?>  ?DATER?
"/>
      <Item4 Name="lazpackager/deb/tpl_control" Value="Source: ?PACKAGE_NAME?
Maintainer: ?MAINTAINER? &lt;?MAINTAINER_EMAIL?>
Section: misc
Priority: optional
Standards-Version: 4.1.4
Build-Depends: fpc, lazarus, lcl, lcl-utils, debhelper (>= 9)

Package: ?PACKAGE_NAME?
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
Description: ?DESCRIPTION?
 ?DESCRIPTION_LONG?
"/>
      <Item5 Name="lazpackager/deb/tpl_copyright" Value="Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: ?COPYRIGHT?
License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 /usr/share/common-licenses/GPL-3
"/>
      <Item6 Name="lazpackager/deb/tpl_rules" Value="#!/usr/bin/make -f

ROOT = $(CURDIR)/debian/?PACKAGE_NAME?

override_dh_auto_clean:
	$(RM) -r lib
	$(RM) lib *.res ?EXECUTABLE?

override_dh_auto_build:
	lazbuild --pcp=/tmp/.lazarus ?PROJECT?

override_dh_auto_install:
	install -d -m 755 $(ROOT)/usr/bin
	install -s -m 755 ?EXECUTABLE? $(ROOT)/usr/bin
	install -d -m 755 $(ROOT)/usr/share/doc/?EXECUTABLE?
	install -m 644 -t $(ROOT)/usr/share/doc/?EXECUTABLE? *.md
	install -d -m 755 $(ROOT)/usr/share/locale/cs/LC_MESSAGES
	install -D -m 644 ?EXECUTABLE?.cs.po $(ROOT)/usr/share/locale/cs/LC_MESSAGES/?EXECUTABLE?.po

%:
	dh $@
"/>
      <Item7 Name="lazpackager/description" Value="A savegame editor for the War in Middle Earth game"/>
      <Item8 Name="lazpackager/description_long" Value="WIMEEd is an editor of saved game files from the War in Middle Earth game published 1988 by Melbourne House and Synergistic Software."/>
      <Item9 Name="lazpackager/export_cmd" Value="?CP? *.lpi ?TEMPFOLDER?/
?CP? *.lpr ?TEMPFOLDER?/
?CP? *.pas ?TEMPFOLDER?/
?CP? *.lfm ?TEMPFOLDER?/
mkdir -p ?TEMPFOLDER?/db
?CP? db/*.pas ?TEMPFOLDER?/db/
?CP? db/*.lfm ?TEMPFOLDER?/db/
?CP? *.ico ?TEMPFOLDER?/
?CP? *.md ?TEMPFOLDER?/
?CP? locale/*.po ?TEMPFOLDER?/"/>
      <Item10 Name="lazpackager/maintainer" Value="Pavel Řezníček"/>
      <Item11 Name="lazpackager/maintainer_email" Value="pavel.reznicek@evangnet.cz"/>
      <Item12 Name="lazpackager/package_name" Value="wimeed"/>
    </CustomData>
    <BuildModes Count="6">
      <Item1 Name="Default" Default="True"/>
      <Item2 Name="Debug">
        <CompilerOptions>
          <Version Value="11"/>
          <PathDelim Value="\"/>
          <Target>
            <Filename Value="wimeed"/>
          </Target>
          <SearchPaths>
            <IncludeFiles Value="$(ProjOutDir)"/>
            <OtherUnitFiles Value="db"/>
            <UnitOutputDirectory Value="lib\$(TargetCPU)-$(TargetOS)"/>
          </SearchPaths>
          <Parsing>
            <SyntaxOptions>
              <IncludeAssertionCode Value="True"/>
            </SyntaxOptions>
          </Parsing>
          <CodeGeneration>
            <Checks>
              <IOChecks Value="True"/>
              <RangeChecks Value="True"/>
              <OverflowChecks Value="True"/>
              <StackChecks Value="True"/>
            </Checks>
            <VerifyObjMethodCallValidity Value="True"/>
          </CodeGeneration>
          <Linking>
            <Debugging>
              <DebugInfoType Value="dsDwarf2Set"/>
              <UseHeaptrc Value="True"/>
              <TrashVariables Value="True"/>
              <UseExternalDbgSyms Value="True"/>
            </Debugging>
            <Options>
              <Win32>
                <GraphicApplication Value="True"/>
              </Win32>
            </Options>
          </Linking>
        </CompilerOptions>
      </Item2>
      <Item3 Name="Release-Win32">
        <CompilerOptions>
          <Version Value="11"/>
          <PathDelim Value="\"/>
          <Target>
            <Filename Value="wimeed"/>
          </Target>
          <SearchPaths>
            <IncludeFiles Value="$(ProjOutDir)"/>
            <OtherUnitFiles Value="db"/>
            <UnitOutputDirectory Value="lib\$(TargetCPU)-$(TargetOS)"/>
          </SearchPaths>
          <CodeGeneration>
            <SmartLinkUnit Value="True"/>
            <TargetCPU Value="i386"/>
            <TargetOS Value="win32"/>
            <Optimizations>
              <OptimizationLevel Value="3"/>
            </Optimizations>
          </CodeGeneration>
          <Linking>
            <Debugging>
              <GenerateDebugInfo Value="False"/>
            </Debugging>
            <LinkSmart Value="True"/>
            <Options>
              <Win32>
                <GraphicApplication Value="True"/>
              </Win32>
            </Options>
          </Linking>
        </CompilerOptions>
      </Item3>
      <Item4 Name="Release-Win64">
        <CompilerOptions>
          <Version Value="11"/>
          <PathDelim Value="\"/>
          <Target>
            <Filename Value="wimeed"/>
          </Target>
          <SearchPaths>
            <IncludeFiles Value="$(ProjOutDir)"/>
            <OtherUnitFiles Value="db"/>
            <UnitOutputDirectory Value="lib\$(TargetCPU)-$(TargetOS)"/>
          </SearchPaths>
          <CodeGeneration>
            <SmartLinkUnit Value="True"/>
            <TargetCPU Value="x86_64"/>
            <TargetOS Value="win64"/>
            <Optimizations>
              <OptimizationLevel Value="3"/>
            </Optimizations>
          </CodeGeneration>
          <Linking>
            <Debugging>
              <GenerateDebugInfo Value="False"/>
            </Debugging>
            <LinkSmart Value="True"/>
            <Options>
              <Win32>
                <GraphicApplication Value="True"/>
              </Win32>
            </Options>
          </Linking>
        </CompilerOptions>
      </Item4>
      <Item5 Name="Release-Lin32">
        <CompilerOptions>
          <Version Value="11"/>
          <PathDelim Value="\"/>
          <Target>
            <Filename Value="Releases\WIMEEd-0.1-$(TargetOS)-$(TargetCPU)\WIMEEd"/>
          </Target>
          <SearchPaths>
            <IncludeFiles Value="$(ProjOutDir)"/>
            <OtherUnitFiles Value="db"/>
            <UnitOutputDirectory Value="lib\$(TargetCPU)-$(TargetOS)"/>
          </SearchPaths>
          <CodeGeneration>
            <SmartLinkUnit Value="True"/>
            <Optimizations>
              <OptimizationLevel Value="3"/>
            </Optimizations>
          </CodeGeneration>
          <Linking>
            <Debugging>
              <GenerateDebugInfo Value="False"/>
            </Debugging>
            <LinkSmart Value="True"/>
            <Options>
              <Win32>
                <GraphicApplication Value="True"/>
              </Win32>
            </Options>
          </Linking>
        </CompilerOptions>
      </Item5>
      <Item6 Name="Release-Lin64">
        <CompilerOptions>
          <Version Value="11"/>
          <PathDelim Value="\"/>
          <Target>
            <Filename Value="wimeed"/>
          </Target>
          <SearchPaths>
            <IncludeFiles Value="$(ProjOutDir)"/>
            <OtherUnitFiles Value="db"/>
            <UnitOutputDirectory Value="lib\$(TargetCPU)-$(TargetOS)"/>
          </SearchPaths>
          <CodeGeneration>
            <SmartLinkUnit Value="True"/>
            <Optimizations>
              <OptimizationLevel Value="3"/>
            </Optimizations>
          </CodeGeneration>
          <Linking>
            <Debugging>
              <GenerateDebugInfo Value="False"/>
            </Debugging>
            <LinkSmart Value="True"/>
            <Options>
              <Win32>
                <GraphicApplication Value="True"/>
              </Win32>
            </Options>
          </Linking>
        </CompilerOptions>
      </Item6>
    </BuildModes>
    <PublishOptions>
      <Version Value="2"/>
    </PublishOptions>
    <RunParams>
      <FormatVersion Value="2"/>
      <Modes Count="1">
        <Mode0 Name="default"/>
      </Modes>
    </RunParams>
    <RequiredPackages Count="1">
      <Item1>
        <PackageName Value="LCL"/>
      </Item1>
    </RequiredPackages>
    <Units Count="12">
      <Unit0>
        <Filename Value="WIMEEd.lpr"/>
        <IsPartOfProject Value="True"/>
      </Unit0>
      <Unit1>
        <Filename Value="main.pas"/>
        <IsPartOfProject Value="True"/>
        <ComponentName Value="FMain"/>
        <HasResources Value="True"/>
        <ResourceBaseClass Value="Form"/>
      </Unit1>
      <Unit2>
        <Filename Value="wimeutils.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMEUtils"/>
      </Unit2>
      <Unit3>
        <Filename Value="db\wimecitynamedb.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMECityNameDB"/>
      </Unit3>
      <Unit4>
        <Filename Value="db\wimecitynamedbobj.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMECityNameDBObj"/>
      </Unit4>
      <Unit5>
        <Filename Value="db\wimedm.pas"/>
        <IsPartOfProject Value="True"/>
        <ComponentName Value="DM"/>
        <HasResources Value="True"/>
        <ResourceBaseClass Value="DataModule"/>
        <UnitName Value="WIMEDM"/>
      </Unit5>
      <Unit6>
        <Filename Value="db\wimemapicondbobj.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMEMapIconDBObj"/>
      </Unit6>
      <Unit7>
        <Filename Value="db\wimemapicondb.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMEMapIconDB"/>
      </Unit7>
      <Unit8>
        <Filename Value="db\wimeinventoryobjectdbobj.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMEInventoryObjectDBObj"/>
      </Unit8>
      <Unit9>
        <Filename Value="db\wimeforcenamedb.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMEForceNameDB"/>
      </Unit9>
      <Unit10>
        <Filename Value="db\wimeinventoryobjectdb.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMEInventoryObjectDB"/>
      </Unit10>
      <Unit11>
        <Filename Value="db\wimeforcenamedbobj.pas"/>
        <IsPartOfProject Value="True"/>
        <UnitName Value="WIMEForceNameDBObj"/>
      </Unit11>
    </Units>
  </ProjectOptions>
  <CompilerOptions>
    <Version Value="11"/>
    <PathDelim Value="\"/>
    <Target>
      <Filename Value="wimeed"/>
    </Target>
    <SearchPaths>
      <IncludeFiles Value="$(ProjOutDir)"/>
      <OtherUnitFiles Value="db"/>
      <UnitOutputDirectory Value="lib\$(TargetCPU)-$(TargetOS)"/>
    </SearchPaths>
    <Linking>
      <Debugging>
        <GenerateDebugInfo Value="False"/>
      </Debugging>
      <Options>
        <Win32>
          <GraphicApplication Value="True"/>
        </Win32>
      </Options>
    </Linking>
  </CompilerOptions>
  <Debugging>
    <Exceptions Count="3">
      <Item1>
        <Name Value="EAbort"/>
      </Item1>
      <Item2>
        <Name Value="ECodetoolError"/>
      </Item2>
      <Item3>
        <Name Value="EFOpenError"/>
      </Item3>
    </Exceptions>
  </Debugging>
</CONFIG>
