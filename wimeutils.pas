unit WIMEUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, WIMECityNameDB, DefaultTranslator;

type
  TEndian = (endLittle, endBig);

  TPlatform = (plPCVGA, plPCEGA, plST, plAmiga, plIIGS);

  TPlatformEndian = array[TPlatform] of TEndian;

  TRawForce = packed record           // Offset:
    CharacterIdentifier: LongWord;    // 0
    CountTotal, CountAlive: Word;     // 4, 6
    LocationX, LocationY: Word;       // 8, 10
    DestinationX, DestinationY: Word; // 12, 14
    InventoryObjects: Word;           // 16
    MapIcon: Byte;                    // 18
    SpriteColorScheme: Byte;          // 19
    SpriteType: Byte;                 // 20
    Unknown21: Byte;                  // 21
    VisibilityIndicator: Byte;        // 22
    Unknown23: Byte;                  // 23
    PowerLevel: Byte;                 // 24
    MoraleTotal: Byte;                // 25
    MoraleQuantity: Byte;             // 26
    Unknown27: Byte;                  // 27
    ArmyMobilizeValue: Byte;          // 28
    Stealth: Byte;                    // 29
    Unknown30, Unknown31: Byte;       // 30, 31
    Unknown32: Byte;                  // 32
    HitPointsTotal,                   // 33
    HitPointsQuantity: Byte;          // 34
    FollowValue: Byte;                // 35
    Unknown36: Byte;                  // 36
  end;                                // 37 bytes total

  TLittleEndianForce = TRawForce;

  TBigEndianForce = packed record
    Force: TRawForce;
    Padding: Byte;                    // 37 (38th byte)
  end;

  TLittleEndianForces = array[0..195] of TLittleEndianForce;
  TBigEndianForces = array[0..195] of TBigEndianForce;

function StrToSed(TheString: String): String;
function BufferToSed(TheBuffer: PChar; Count: Integer): String;
function PadInt(TheInt: Integer; PlaceCount: Integer): String;
function SwapForceEndian(Force: TRawForce): TRawForce;

operator:=(Force: TLittleEndianForce): TBigEndianForce;
operator:=(Force: TBigEndianForce): TLittleEndianForce;
operator:=(Forces: TBigEndianForces): TLittleEndianForces;
operator:=(Forces: TLittleEndianForces): TBigEndianForces;

implementation

function StrToSed(TheString: String): String;
var
  I: integer;
  Ch: char;
  B: byte absolute Ch;
begin
  Result := '';
  for I := 1 to Length(TheString) do
  begin
    Ch := TheString[I];
    Result := Result + IntToHex(integer(B), 2) + ' ';
  end;
end;

function BufferToSed(TheBuffer: PChar; Count: Integer): String;
var
  I: integer;
  Ch: char;
  B: byte absolute Ch;
begin
  Result := '';
  for I := 0 to Count - 1 do
  begin
    Ch := TheBuffer[I];
    Result := Result + IntToHex(integer(B), 2) + ' ';
  end;
end;

function PadInt(TheInt: Integer; PlaceCount: Integer): String;
var
  BaseString: String;
  BaseStrLength: Integer;
  Zeroes: String;
  ZeroCount: Integer;
begin
  BaseString := IntToStr(TheInt);
  BaseStrLength := Length(BaseString);
  if PlaceCount > BaseStrLength then
  begin
    ZeroCount := PlaceCount - BaseStrLength;
    Zeroes := StringOfChar('0', ZeroCount);
    Result := Zeroes + BaseString;
  end
  else
    Result := BaseString;
end;


function SwapForceEndian(Force: TRawForce): TRawForce;
begin
  Result.CharacterIdentifier := SwapEndian(Force.CharacterIdentifier);
  Result.ArmyMobilizeValue := Force.ArmyMobilizeValue;
  Result.CountAlive := SwapEndian(Force.CountAlive);
  Result.CountTotal := SwapEndian(Force.CountTotal);
  Result.DestinationX := SwapEndian(Force.DestinationX);
  Result.DestinationY := SwapEndian(Force.DestinationY);
  Result.FollowValue := Force.FollowValue;
  Result.HitPointsQuantity := Force.HitPointsQuantity;
  Result.HitPointsTotal := Force.HitPointsTotal;
  Result.InventoryObjects := Force.InventoryObjects;
  Result.LocationX := SwapEndian(Force.LocationX);
  Result.LocationY := SwapEndian(Force.LocationY);
  Result.MapIcon := Force.MapIcon;
  Result.MoraleQuantity := Force.MoraleQuantity;
  Result.MoraleTotal := Force.MoraleTotal;
  Result.PowerLevel := Force.PowerLevel;
  Result.SpriteColorScheme := Force.SpriteColorScheme;
  Result.SpriteType := Force.SpriteType;
  Result.Stealth := Force.Stealth;
  Result.Unknown21 := Force.Unknown21;
  Result.Unknown23 := Force.Unknown23;
  Result.Unknown27 := Force.Unknown27;
  Result.Unknown30 := Force.Unknown30;
  Result.Unknown31 := Force.Unknown31;
  Result.Unknown32 := Force.Unknown32;
  Result.Unknown36 := Force.Unknown36;
  Result.VisibilityIndicator := Force.VisibilityIndicator;
end;

operator:=(Force: TLittleEndianForce): TBigEndianForce;
begin
  Result.Force := SwapForceEndian(Force);
  Result.Padding := 0;
end;

operator:=(Force: TBigEndianForce): TLittleEndianForce;
begin
  Result := SwapForceEndian(Force.Force);
end;

operator:=(Forces: TLittleEndianForces): TBigEndianForces;
var
  F: Integer;
begin
  for F := Low(Result) to High(Result) do
    Result[F] := Forces[F];
end;

operator:=(Forces: TBigEndianForces): TLittleEndianForces;
var
  F: Integer;
begin
  for F := Low(Result) to High(Result) do
    Result[F] := Forces[F];
end;


end.

