program WIMEEd;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  main,
  WIMEDM,
  WIMEUtils,
  WIMECityNameDB, WIMECityNameDBObj,
  WIMEMapIconDB, WIMEMapIconDBObj,
  WIMEInventoryObjectDB, WIMEInventoryObjectDBObj,
  WIMEForceNameDB
  { you can add units after this },
  DefaultTranslator, WIMEForceNameDBObj;

{$R *.res}

begin
  //RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TFMain, FMain);
  Application.Run;
end.

